# Canteen Management System

`Canteen Management System` is a **javascript** project which uses:
**ReactJS** for _frontend_ &
**ExpressJS** for _backend_

`Canteen Management System` features

- Authentication
- Role based Authorization
- Ordering food
- Payment through administration
- User account and account credit system
- Multilevel Category(s) and SubCategory(s)
- Product Pagination and Filtering
- Product Searching powered by Meilisearch

## Getting Started

### Clone the repository

```

git clone https://gitlab.com/dipsagungurung/canteen-management-system.git

```

##

### Install the node packages

```

cd client

npm install

cd ..

cd server

npm install

```

> `Canteen Management System` project uses client/server folder structure so to install the neccessary packages run the install script in both `client` and `server` directory.

##

### Create .env file

> The cloned project consists an `example.env` file for both `client` and `server` directory, **create a `.env` file** with the your credentials as per the example file inside `client` and `server` directory

##

### Running the project (development environment)

#### To run frontend of the project

```

cd client

npm run dev

```

#### To run backend of the project

```

cd server

npm run dev

```

##

### Seeding the project (optional)

```

cd server

npm run seed

```

> The seed script seeds the project with dummy data to play with

## Thank you for visiting 💖
