const axios = require("axios").default;

const instance = axios.create({
  baseURL: "http://localhost:8080/",
  timeout: 1000,
  //   headers: { "Content-Type": "application/json" },
});

if (typeof window !== "undefined") {
  const TOKEN = localStorage.getItem("jwt-token");
  if (TOKEN) {
    const BEARER_TOKEN = "Bearer " + TOKEN;
    instance.defaults.headers.common["Authorization"] = BEARER_TOKEN;
  }
}

export default instance;
