import { useContext, useEffect } from "react";
import { UserContext } from "../pages/_app";
import { useRouter } from "next/router";
import Link from "next/link";

const AdminLayout = (props) => {
  const [user, setUserData] = useContext(UserContext);
  const router = useRouter();

  useEffect(() => {
    if (!user) {
      router.push("/admin");
    }
    if (user) {
      let userRoles = user.roles.map((currentValue, index) => {
        return currentValue.role_name;
      });
      if (
        !(
          userRoles.includes("admin") ||
          userRoles.includes("manager") ||
          userRoles.includes("staff")
        )
      ) {
        localStorage.removeItem("jwt-token");
        router.push("/admin");
      }
    }
  }, [router, user]);

  const logOutHandler = () => {
    setUserData(null);
    localStorage.removeItem("jwt-token");
    router.push("/admin");
  };

  return (
    <div className="w-full min-h-screen">
      <div className="mx-auto grid grid-cols-7">
        <nav className="h-screen col-span-1 flex flex-col justify-start items-start min-w-fit bg-neutral-200 divide-y-2 divide-gray-400 divide-opacity-60">
          <h4 className="w-full text-center font-semibold text-lg px-3 py-2">
            Canteen Management System
          </h4>
          <div className="w-full px-3 py-2 text-center font-semibold hover:bg-neutral-300 cursor-pointer">
            {user ? user.username : "User"}
          </div>
          <div className="w-full flex flex-col justify-center items-start">
            {LayoutLinks.map((currentValue, index) => {
              return (
                <Link key={index} href={currentValue.link} passHref>
                  <div className="w-full px-5 py-2 hover:bg-neutral-300 cursor-pointer">
                    {currentValue.title}
                  </div>
                </Link>
              );
            })}
          </div>
          <div
            className="w-full px-3 py-2 text-center font-semibold hover:bg-neutral-300 cursor-pointer"
            onClick={logOutHandler}
          >
            Logout
          </div>
        </nav>
        <div className="col-span-6">{props.children}</div>
      </div>
    </div>
  );
};

const LayoutLinks = [
  {
    title: "Dashboard",
    link: "/admin/dashboard",
  },
  {
    title: "Module",
    link: "/admin/modules",
  },
  {
    title: "Privileges",
    link: "/admin/privileges",
  },
  {
    title: "Manage Module Privileges",
    link: "/admin/manageModulePrivileges",
  },
  {
    title: "Roles",
    link: "/admin/roles",
  },
  {
    title: "Manage Role Access",
    link: "/admin/manageRoleAccess",
  },
  {
    title: "Users",
    link: "/admin/users",
  },
  {
    title: "Manage User Roles",
    link: "/admin/manageUserRoles",
  },
  {
    title: "Categories",
    link: "/admin/categories",
  },
  {
    title: "Products",
    link: "/admin/products",
  },
  {
    title: "Orders",
    link: "/admin/orders",
  },
  {
    title: "Payments",
    link: "/admin/payments",
  },
];

export default AdminLayout;
