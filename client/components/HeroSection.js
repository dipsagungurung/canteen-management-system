import ProductCard from "./ProductCard";

const HeroSection = () => {
  return (
    <div className="w-full pt-6 ">
      <div className="w-full grid grid-cols-5">
        {/* Categories Section */}
        <div className="col-span-1 bg-white border-2">
          <div className="w-full px-3 py-2 text-lg font-semibold border-b-2">
            Categories
          </div>
          <div className="w-full px-3 py-2 hover:bg-slate-100 cursor-pointer">
            Drinks
          </div>
          <div className="w-full px-3 py-2 hover:bg-slate-100 cursor-pointer">
            Snacks
          </div>
        </div>

        {/* Products Section */}
        <div className="col-span-4">
          <div className="w-full px-8">
            <h3 className="text-lg font-semibold py-2">Products</h3>
            <div className="w-full grid grid-cols-3">
              <ProductCard />
              <ProductCard />
              <ProductCard />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HeroSection;
