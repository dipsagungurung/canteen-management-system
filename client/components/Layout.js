import Navbar from "./Navbar";

const Layout = (props) => {
  return (
    <div className="max-w-7xl mx-auto min-h-screen">
      <Navbar />
      {props.children}
    </div>
  );
};

export default Layout;
