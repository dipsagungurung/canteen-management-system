import { useContext } from "react";
import Link from "next/link";
import { UserContext } from "../pages/_app";
import { useRouter } from "next/router";

const Navbar = () => {
  const [user, setUserData] = useContext(UserContext);
  const router = useRouter();

  const logOutHandler = () => {
    setUserData(null);
    localStorage.removeItem("jwt-token");
    router.push("/");
  };

  let navLinks = (
    <div className="">
      <ul className="flex items-center">
        <li className="underline underline-offset-4">
          <Link href="/signIn">Sign in</Link>
        </li>

        <li className="ml-5 underline underline-offset-4">
          <Link href="/signUp">Register</Link>
        </li>
      </ul>
    </div>
  );

  if (user) {
    navLinks = (
      <div className="">
        <ul className="flex items-center">
          <li className="underline underline-offset-4">
            <Link href="/account">My Account</Link>
          </li>

          <li className="ml-5 underline underline-offset-4">
            <Link href="/payments">My Payments</Link>
          </li>

          <li className="ml-5 underline underline-offset-4">
            <Link href="/orders">My Orders</Link>
          </li>

          <li className="ml-5 underline underline-offset-4">
            <Link href="/profile">{user.username}</Link>
          </li>

          <li
            className="ml-5 underline underline-offset-4 cursor-pointer"
            onClick={logOutHandler}
          >
            Logout
          </li>
        </ul>
      </div>
    );
  }
  return (
    <nav className="w-full border-b-2">
      {/* Logo & Website authentication links */}
      <div className="w-full  flex items-center justify-between px-7 py-5">
        <div className="font-bold text-lg">
          <Link href="/">Canteen Management System</Link>
        </div>

        {/* Dynamic nav links if user is logged in */}
        {navLinks}
      </div>
    </nav>
  );
};

export default Navbar;
