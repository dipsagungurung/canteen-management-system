import { useState, createContext } from "react";
import "../styles/globals.css";

export const UserContext = createContext();

function MyApp({ Component, pageProps }) {
  const [userData, setUserData] = useState(null);

  return (
    <UserContext.Provider value={[userData, setUserData]}>
      <div className="bg-gray-100">
        <Component {...pageProps} />
      </div>
    </UserContext.Provider>
  );
}

export default MyApp;
