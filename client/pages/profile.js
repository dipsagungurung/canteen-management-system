import Layout from "../components/Layout";
import { useContext } from "react";
import { UserContext } from "./_app";

const Profile = () => {
  const [userData] = useContext(UserContext);

  let profileBody;

  profileBody = (
    <div className="w-full pt-6 ">
      <h4 className="font-semibold text-lg text-opacity-70 mb-5">My Account</h4>
      <div>
        <p>Sorry! Could not find the profile</p>
      </div>
    </div>
  );

  if (userData) {
    profileBody = (
      <div className="w-full pt-6 ">
        <h4 className="font-semibold text-lg text-opacity-70 mb-5">
          My Profile
        </h4>
        <div>
          <p>Username: {userData.username}</p>
          <p>Email: {userData.username}</p>
          <p>Joined date: {userData.createdAt}</p>
          <p>Credit balance: {userData.account.credit_balance}</p>
        </div>
      </div>
    );
  }
  return (
    <Layout>
      <div className="w-full pt-6 ">{profileBody}</div>
    </Layout>
  );
};

export default Profile;
