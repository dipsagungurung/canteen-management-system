import { useRouter } from "next/router";
import { useState, useContext } from "react";
import axios from "../../utils/axios";
import { UserContext } from "../_app";

const Index = () => {
  const [userData, setUserData] = useContext(UserContext);
  const router = useRouter();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const onSubmitHandler = async (e) => {
    try {
      e.preventDefault();
      const loginResponse = await axios.post("/login", {
        email: email,
        password: password,
      });
      localStorage.setItem("jwt-token", loginResponse.data.data.token);
      const TOKEN = localStorage.getItem("jwt-token");
      if (TOKEN) {
        const BEARER_TOKEN = "Bearer " + TOKEN;
        axios.defaults.headers.common["Authorization"] = BEARER_TOKEN;
      }
      const profileResponse = await axios.get("/profile");
      setUserData(profileResponse.data.data);
      console.log(profileResponse.data.data);
      router.push("/admin/dashboard");
    } catch (err) {
      console.log(err.response.data);
    }
  };

  return (
    <div className="max-w-7xl mx-auto h-screen flex items-center justify-center">
      <div className="w-full flex items-center justify-center">
        <div className="bg-white border-black border-2 rounded-lg border-opacity-40 px-10 py-16 ">
          <h4 className="font-semibold text-xl mb-6 text-center underline underline-offset-2">
            Canteen Management System
          </h4>
          <form
            className="w-full space-y-3"
            method="POST"
            onSubmit={(e) => onSubmitHandler(e)}
          >
            <div className="w-full flex items-center justify-between space-x-10">
              <label htmlFor="email">Email</label>
              <input
                type="email"
                id="email"
                name="email"
                placeholder="Enter your email."
                className="border-2 border-black border-opacity-50 px-2 py-2 rounded-lg"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>
            <div className="w-full flex items-center justify-between space-x-10">
              <label htmlFor="password">Password</label>
              <input
                type="password"
                id="password"
                name="password"
                placeholder="Enter your password."
                className="border-2 border-black border-opacity-50 px-2 py-2 rounded-lg"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </div>
            <div className="w-full flex items-center justify-center ">
              <button
                type="submit"
                className="mt-4 px-4 py-2 bg-blue-400 rounded-lg"
              >
                Submit
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Index;
