import AdminLayout from "../../components/AdminLayout";
import { useState, useEffect } from "react";
import axios from "../../utils/axios";

const Privileges = () => {
  const [privileges, setPrivileges] = useState([]);
  const [loading, setLoading] = useState(false);
  const [privilegeName, setPrivilegeName] = useState("");
  const [privilegeMethod, setPrivilegeMethod] = useState("");

  useEffect(() => {
    fetchPrivileges();
  }, []);

  const fetchPrivileges = async () => {
    try {
      setLoading(true);
      const response = await axios.get("/privileges");
      setPrivileges(response.data.data);
      setLoading(false);
    } catch (err) {
      setLoading(false);
      console.log(err.response.data);
    }
  };

  const createPrivilege = async (e) => {
    try {
      e.preventDefault();
      await axios.post("/privileges", {
        privilege_name: privilegeName,
        privilege_method: privilegeMethod,
      });
      setPrivilegeName("");
      setPrivilegeMethod("");
      fetchPrivileges();
    } catch (err) {
      console.log(err.response.data);
    }
  };

  const deletePrivilege = async (privilegeId) => {
    try {
      await axios.delete(`/privileges/${privilegeId}`);
      fetchPrivileges();
    } catch (err) {
      console.log(err.response.data);
    }
  };

  return (
    <AdminLayout>
      <div className="w-8/12 mx-auto">
        {/* Creating privilege */}
        <div className="mx-auto px-4 py-4">
          <div className="bg-white border-black border-2 rounded-lg border-opacity-40 px-10 py-16 ">
            <h4 className="font-semibold text-xl mb-6 text-center underline underline-offset-2">
              Create Privilege
            </h4>
            <form
              className="w-full space-y-3"
              method="POST"
              onSubmit={(e) => createPrivilege(e)}
            >
              <div className="w-full flex items-center justify-between space-x-10">
                <label htmlFor="p_name">Privilege name</label>
                <input
                  type="text"
                  id="p_name"
                  name="p_name"
                  placeholder="Enter privilege name."
                  className="border-2 border-black border-opacity-50 px-2 py-2 rounded-lg"
                  value={privilegeName}
                  onChange={(e) => setPrivilegeName(e.target.value)}
                />
              </div>
              <div className="w-full flex items-center justify-between space-x-10">
                <label htmlFor="p_method">Privilege method</label>
                <input
                  type="text"
                  id="p_method"
                  name="p_method"
                  placeholder="Enter your privilege method"
                  className="border-2 border-black border-opacity-50 px-2 py-2 rounded-lg"
                  value={privilegeMethod}
                  onChange={(e) => setPrivilegeMethod(e.target.value)}
                />
              </div>
              <div className="w-full flex items-center justify-center ">
                <button
                  type="submit"
                  className="mt-4 px-4 py-2 bg-blue-400 rounded-lg"
                >
                  Create
                </button>
              </div>
            </form>
          </div>
        </div>

        {/* Displaying */}
        {loading ? null : (
          <div className="w-full">
            <div className="w-full px-4 py-4">
              <p className="text-lg font-semibold underline underline-offset-1 mb-3">
                Privileges
              </p>
              <div className="w-full space-y-5">
                {privileges &&
                  privileges.map((currValue, index) => {
                    return (
                      <div
                        key={currValue.privilege_id}
                        className="px-2 py-2 flex justify-between items-center border border-opacity-70 rounded-md  bg-neutral-200"
                      >
                        <p>
                          Id:{" "}
                          <span className="font-semibold">
                            {currValue.privilege_id}
                          </span>
                        </p>
                        <p>
                          Privilege name:{" "}
                          <span className="font-semibold">
                            {currValue.privilege_name}
                          </span>
                        </p>
                        <p>
                          Privilege method:{" "}
                          <span className="font-semibold">
                            {currValue.privilege_method}
                          </span>
                        </p>
                        <button
                          type="button"
                          className="bg-red-400 rounded-md px-2 py-1"
                          onClick={() =>
                            deletePrivilege(currValue.privilege_id)
                          }
                        >
                          Delete
                        </button>
                      </div>
                    );
                  })}
              </div>
            </div>
          </div>
        )}
      </div>
    </AdminLayout>
  );
};

export default Privileges;
