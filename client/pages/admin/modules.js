import AdminLayout from "../../components/AdminLayout";
import { useState, useEffect } from "react";
import axios from "../../utils/axios";

const Modules = () => {
  const [modules, setModules] = useState([]);
  const [loading, setLoading] = useState(false);
  const [moduleName, setModuleName] = useState("");
  const [moduleEndpoint, setModuleEndpoint] = useState("");

  useEffect(() => {
    fetchModules();
  }, []);

  const fetchModules = async () => {
    try {
      setLoading(true);
      const response = await axios.get("/modules");
      setModules(response.data.data);
      setLoading(false);
    } catch (err) {
      setLoading(false);
      console.log(err.response.data);
    }
  };

  const createModule = async (e) => {
    try {
      e.preventDefault();
      await axios.post("/modules", {
        module_name: moduleName,
        module_endpoint: moduleEndpoint,
      });
      setModuleName("");
      setModuleEndpoint("");
      fetchModules();
    } catch (err) {
      console.log(err.response.data);
    }
  };

  const deleteModule = async (moduleId) => {
    try {
      await axios.delete(`/modules/${moduleId}`);
      fetchModules();
    } catch (err) {
      console.log(err.response.data);
    }
  };

  return (
    <AdminLayout>
      <div className="w-8/12 mx-auto">
        {/* Creating module */}
        <div className="mx-auto px-4 py-4">
          <div className="bg-white border-black border-2 rounded-lg border-opacity-40 px-10 py-16 ">
            <h4 className="font-semibold text-xl mb-6 text-center underline underline-offset-2">
              Create Module
            </h4>
            <form
              className="w-full space-y-3"
              method="POST"
              onSubmit={(e) => createModule(e)}
            >
              <div className="w-full flex items-center justify-between space-x-10">
                <label htmlFor="m_name">Module name</label>
                <input
                  type="text"
                  id="m_name"
                  name="m_name"
                  placeholder="Enter module name."
                  className="border-2 border-black border-opacity-50 px-2 py-2 rounded-lg"
                  value={moduleName}
                  onChange={(e) => setModuleName(e.target.value)}
                />
              </div>
              <div className="w-full flex items-center justify-between space-x-10">
                <label htmlFor="m_endpoint">Module Endpoint</label>
                <input
                  type="text"
                  id="m_endpoint"
                  name="m_endpoint"
                  placeholder="Enter your module endpoint."
                  className="border-2 border-black border-opacity-50 px-2 py-2 rounded-lg"
                  value={moduleEndpoint}
                  onChange={(e) => setModuleEndpoint(e.target.value)}
                />
              </div>
              <div className="w-full flex items-center justify-center ">
                <button
                  type="submit"
                  className="mt-4 px-4 py-2 bg-blue-400 rounded-lg"
                >
                  Create
                </button>
              </div>
            </form>
          </div>
        </div>

        {/* Displaying */}
        {loading ? null : (
          <div className="w-full">
            <div className="w-full px-4 py-4">
              <p className="text-lg font-semibold underline underline-offset-1 mb-3">
                Modules
              </p>
              <div className="w-full space-y-5">
                {modules &&
                  modules.map((currValue, index) => {
                    return (
                      <div
                        key={currValue.module_id}
                        className="px-2 py-2 flex justify-between items-center border border-opacity-70 rounded-md  bg-neutral-200"
                      >
                        <p>
                          Id:{" "}
                          <span className="font-semibold">
                            {currValue.module_id}
                          </span>
                        </p>
                        <p>
                          Module name:{" "}
                          <span className="font-semibold">
                            {currValue.module_name}
                          </span>
                        </p>
                        <p>
                          Module endpoint:{" "}
                          <span className="font-semibold">
                            {currValue.module_endpoint}
                          </span>
                        </p>
                        <button
                          type="button"
                          className="bg-red-400 rounded-md px-2 py-1"
                          onClick={() => deleteModule(currValue.module_id)}
                        >
                          Delete
                        </button>
                      </div>
                    );
                  })}
              </div>
            </div>
          </div>
        )}
      </div>
    </AdminLayout>
  );
};

export default Modules;
