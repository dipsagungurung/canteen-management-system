import AdminLayout from "../../components/AdminLayout";
import { useState, useEffect } from "react";
import axios from "../../utils/axios";

const Users = () => {
  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    fetchUsers();
  }, []);

  const fetchUsers = async () => {
    try {
      setLoading(true);
      const response = await axios.get("/users");
      setUsers(response.data.data);
      setLoading(false);
    } catch (err) {
      setLoading(false);
      console.log(err.response.data);
    }
  };

  const deleteUser = async (userId) => {
    try {
      await axios.delete(`/users/${userId}`);
      fetchUsers();
    } catch (err) {
      console.log(err.response.data);
    }
  };

  return (
    <AdminLayout>
      <div className="w-8/12 mx-auto">
        {/* Displaying */}
        {loading ? null : (
          <div className="w-full">
            <div className="w-full px-4 py-4">
              <p className="text-lg font-semibold underline underline-offset-1 mb-3">
                Users
              </p>
              <div className="w-full space-y-5">
                {users &&
                  users.map((currValue, index) => {
                    return (
                      <div
                        key={currValue.id}
                        className="px-2 py-2 flex justify-between items-center border border-opacity-70 rounded-md  bg-neutral-200"
                      >
                        <div className="space-y-2">
                          <p>
                            Id:{" "}
                            <span className="font-semibold">
                              {currValue.id}
                            </span>
                          </p>
                          <p>
                            Username:{" "}
                            <span className="font-semibold">
                              {currValue.username}
                            </span>
                          </p>
                          <p>
                            Email:{" "}
                            <span className="font-semibold">
                              {currValue.email}
                            </span>
                          </p>
                          <p>
                            Roles:{" "}
                            <span className="font-semibold">
                              {currValue.roles.map((item) => {
                                return `${item.role_name}, `;
                              })}
                            </span>
                          </p>
                        </div>
                        <div className="space-x-3">
                          <button
                            type="button"
                            className="bg-blue-400 rounded-md px-2 py-1"
                            onClick={() => deleteUser(currValue.role_id)}
                          >
                            View
                          </button>
                          <button
                            type="button"
                            className="bg-blue-400 rounded-md px-2 py-1"
                            onClick={() => deleteUser(currValue.role_id)}
                          >
                            Edit
                          </button>
                          <button
                            type="button"
                            className="bg-red-400 rounded-md px-2 py-1"
                            onClick={() => deleteUser(currValue.role_id)}
                          >
                            Delete
                          </button>
                        </div>
                      </div>
                    );
                  })}
              </div>
            </div>
          </div>
        )}
      </div>
    </AdminLayout>
  );
};

export default Users;
