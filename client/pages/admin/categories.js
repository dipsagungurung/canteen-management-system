import AdminLayout from "../../components/AdminLayout";
import { useState, useEffect } from "react";
import axios from "../../utils/axios";

const Categories = () => {
  const [categories, setCategories] = useState([]);
  const [loading, setLoading] = useState(false);
  const [categoryName, setCategoryName] = useState("");
  const [categoryDescription, setCategoryDescription] = useState("");

  useEffect(() => {
    fetchCategories();
  }, []);

  const fetchCategories = async () => {
    try {
      setLoading(true);
      const response = await axios.get("/categories");
      setCategories(response.data.data);
      setLoading(false);
    } catch (err) {
      setLoading(false);
      console.log(err.response.data);
    }
  };

  const createCategory = async (e) => {
    try {
      e.preventDefault();
      await axios.post("/categories", {
        category_name: categoryName,
        category_description: categoryDescription,
      });
      setCategoryName("");
      setCategoryDescription("");
      fetchCategories();
    } catch (err) {
      console.log(err.response.data);
    }
  };

  const editCategory = async (categoryId) => {};

  const deleteCategory = async (categoryId) => {
    try {
      await axios.delete(`/categories/${categoryId}`);
      fetchCategories();
    } catch (err) {
      console.log(err.response.data);
    }
  };

  return (
    <AdminLayout>
      <div className="w-8/12 mx-auto">
        {/* Creating Category */}
        <div className="mx-auto px-4 py-4">
          <div className="bg-white border-black border-2 rounded-lg border-opacity-40 px-10 py-16 ">
            <h4 className="font-semibold text-xl mb-6 text-center underline underline-offset-2">
              Create Category
            </h4>
            <form
              className="w-full space-y-3"
              method="POST"
              onSubmit={(e) => createCategory(e)}
            >
              <div className="w-full flex items-center justify-between space-x-10">
                <label htmlFor="c_name">Category name</label>
                <input
                  type="text"
                  id="c_name"
                  name="c_name"
                  placeholder="Enter category name."
                  className="border-2 border-black border-opacity-50 px-2 py-2 rounded-lg"
                  value={categoryName}
                  onChange={(e) => setCategoryName(e.target.value)}
                />
              </div>
              <div className="w-full flex items-center justify-between space-x-10">
                <label htmlFor="c_description">Category Description</label>
                <input
                  type="text"
                  id="c_description"
                  name="c_description"
                  placeholder="Enter your privilege method"
                  className="border-2 border-black border-opacity-50 px-2 py-2 rounded-lg"
                  value={categoryDescription}
                  onChange={(e) => setCategoryDescription(e.target.value)}
                />
              </div>
              <div className="w-full flex items-center justify-center ">
                <button
                  type="submit"
                  className="mt-4 px-4 py-2 bg-blue-400 rounded-lg"
                >
                  Create
                </button>
              </div>
            </form>
          </div>
        </div>

        {/* Displaying */}
        {loading ? null : (
          <div className="w-full">
            <div className="w-full px-4 py-4">
              <p className="text-lg font-semibold underline underline-offset-1 mb-3">
                Categories
              </p>
              <div className="w-full space-y-5">
                {categories &&
                  categories.map((currValue, index) => {
                    return (
                      <div
                        key={currValue.category_id}
                        className="px-2 py-2 flex justify-between items-center border border-opacity-70 rounded-md  bg-neutral-200"
                      >
                        <div className="space-y-2">
                          <p>
                            Id:{" "}
                            <span className="font-semibold">
                              {currValue.category_id}
                            </span>
                          </p>
                          <p>
                            Category name:{" "}
                            <span className="font-semibold">
                              {currValue.category_name}
                            </span>
                          </p>
                        </div>
                        <div className="space-x-3">
                          <button
                            type="button"
                            className="bg-blue-400 rounded-md px-2 py-1"
                            onClick={() => editCategory(currValue.category_id)}
                          >
                            Edit
                          </button>
                          <button
                            type="button"
                            className="bg-red-400 rounded-md px-2 py-1"
                            onClick={() =>
                              deleteCategory(currValue.category_id)
                            }
                          >
                            Delete
                          </button>
                        </div>
                      </div>
                    );
                  })}
              </div>
            </div>
          </div>
        )}
      </div>
    </AdminLayout>
  );
};

export default Categories;
