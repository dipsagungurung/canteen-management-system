import AdminLayout from "../../components/AdminLayout";
import { useState, useEffect } from "react";
import axios from "../../utils/axios";

const Orders = () => {
  const [orders, setOrders] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    fetchOrders();
  }, []);

  const fetchOrders = async () => {
    try {
      setLoading(true);
      const response = await axios.get("/orders");
      setOrders(response.data.data);
      setLoading(false);
    } catch (err) {
      setLoading(false);
      console.log(err.response.data);
    }
  };

  const editOrder = async (orderId) => {};

  const deleteOrder = async (orderId) => {
    try {
      await axios.delete(`/orders/${orderId}`);
      fetchOrders();
    } catch (err) {
      console.log(err.response.data);
    }
  };

  return (
    <AdminLayout>
      <div className="w-8/12 mx-auto">
        {/* Displaying */}
        {loading ? null : (
          <div className="w-full">
            <div className="w-full px-4 py-4">
              <p className="text-lg font-semibold underline underline-offset-1 mb-3">
                Orders
              </p>
              <div className="w-full space-y-5">
                {orders &&
                  orders.map((currValue, index) => {
                    return (
                      <div
                        key={currValue.order_id}
                        className="px-2 py-2 flex justify-between items-center border border-opacity-70 rounded-md  bg-neutral-200"
                      >
                        <div className="space-y-2">
                          <p>
                            Id:{" "}
                            <span className="font-semibold">
                              {currValue.order_id}
                            </span>
                          </p>
                          <p>
                            Ordered By:{" "}
                            <span className="font-semibold">
                              {currValue.user_id}
                            </span>
                          </p>
                          <p>
                            Order Status:{" "}
                            <span className="font-semibold">
                              {currValue.order_status}
                            </span>
                          </p>
                          <p>
                            Ordered At:{" "}
                            <span className="font-semibold">
                              {currValue.createdAt}
                            </span>
                          </p>
                        </div>
                        <div className="space-x-3">
                          <button
                            type="button"
                            className="bg-blue-400 rounded-md px-2 py-1"
                            onClick={() => deleteOrder(currValue.module_id)}
                          >
                            View
                          </button>
                          <button
                            type="button"
                            className="bg-blue-400 rounded-md px-2 py-1"
                            onClick={() => editOrder(currValue.module_id)}
                          >
                            Edit
                          </button>
                          <button
                            type="button"
                            className="bg-red-400 rounded-md px-2 py-1"
                            onClick={() => deleteOrder(currValue.module_id)}
                          >
                            Delete
                          </button>
                        </div>
                      </div>
                    );
                  })}
              </div>
            </div>
          </div>
        )}
      </div>
    </AdminLayout>
  );
};

export default Orders;
