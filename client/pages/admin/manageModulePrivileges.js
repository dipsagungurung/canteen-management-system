import AdminLayout from "../../components/AdminLayout";
import { useState, useEffect } from "react";
import axios from "../../utils/axios";

const ManageModulePrivileges = () => {
  const [modules, setModules] = useState([]);
  const [privileges, setPrivileges] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    fetchModules();
    fetchPrivileges();
  }, []);

  const fetchModules = async () => {
    try {
      setLoading(true);
      const response = await axios.get("/modules");
      setModules(response.data.data);
      setLoading(false);
    } catch (err) {
      setLoading(false);
      console.log(err.response.data);
    }
  };
  const fetchPrivileges = async () => {
    try {
      setLoading(true);
      const response = await axios.get("/privileges");
      setPrivileges(response.data.data);
      setLoading(false);
    } catch (err) {
      setLoading(false);
      console.log(err.response.data);
    }
  };

  // const createRole = async (e) => {
  //   try {
  //     e.preventDefault();
  //     await axios.post("/roles", {
  //       role_name: roleName,
  //     });
  //     setRoleName("");
  //     fetchModules();
  //   } catch (err) {
  //     console.log(err.response.data);
  //   }
  // };

  const deleteRole = async (roleId) => {
    try {
      await axios.delete(`/roles/${roleId}`);
      fetchModules();
    } catch (err) {
      console.log(err.response.data);
    }
  };

  return (
    <AdminLayout>
      <div className="w-8/12 mx-auto">
        {/* Displaying */}
        {loading ? null : (
          <div className="w-full">
            <div className="w-full px-4 py-4">
              <div className="text-lg font-semibold underline underline-offset-1 mb-3 flex items-center justify-between">
                <p>Modules</p>
                <p>Privileges</p>
              </div>
              <div className="w-full space-y-5">
                {modules &&
                  modules.map((currValue, index) => {
                    return (
                      <div
                        key={currValue.module_id}
                        className="px-2 py-2 flex justify-between items-center border border-opacity-70 rounded-md  bg-neutral-200"
                      >
                        <div className="space-y-2">
                          <p>
                            Id:{" "}
                            <span className="font-semibold">
                              {currValue.module_id}
                            </span>
                          </p>
                          <p>
                            Module name:{" "}
                            <span className="font-semibold">
                              {currValue.module_name}
                            </span>
                          </p>
                          <p>
                            Module endpoint:{" "}
                            <span className="font-semibold">
                              {currValue.module_endpoint}
                            </span>
                          </p>
                        </div>
                        <div className="space-x-3">
                          <form className="w-full">
                            {privileges.map((privilege) => {
                              return (
                                <div
                                  key={privilege.privilege_id}
                                  className="flex items-center"
                                >
                                  <input
                                    type="checkbox"
                                    id={privilege.privilege_name}
                                    name={privilege.privilege_name}
                                  />
                                  <label htmlFor={privilege.privilege_name}>
                                    {privilege.privilege_name}
                                  </label>
                                </div>
                              );
                            })}
                            <button
                              type="submit"
                              className="bg-blue-400 rounded-md px-2 py-1"
                            >
                              Save
                            </button>
                          </form>
                        </div>
                      </div>
                    );
                  })}
              </div>
            </div>
          </div>
        )}
      </div>
    </AdminLayout>
  );
};

export default ManageModulePrivileges;
