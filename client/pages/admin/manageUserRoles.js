import AdminLayout from "../../components/AdminLayout";
import { useState, useEffect } from "react";
import axios from "../../utils/axios";

const ManageUserRoles = () => {
  const [users, setUsers] = useState([]);
  const [roles, setRoles] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    fetchUsers();
    fetchRoles();
  }, []);

  const fetchUsers = async () => {
    try {
      setLoading(true);
      const response = await axios.get("/users");
      setUsers(response.data.data);
      setLoading(false);
    } catch (err) {
      setLoading(false);
      console.log(err.response.data);
    }
  };
  const fetchRoles = async () => {
    try {
      setLoading(true);
      const response = await axios.get("/roles");
      setRoles(response.data.data);
      setLoading(false);
    } catch (err) {
      setLoading(false);
      console.log(err.response.data);
    }
  };

  // const createRole = async (e) => {
  //   try {
  //     e.preventDefault();
  //     await axios.post("/roles", {
  //       role_name: roleName,
  //     });
  //     setRoleName("");
  //     fetchUsers();
  //   } catch (err) {
  //     console.log(err.response.data);
  //   }
  // };

  return (
    <AdminLayout>
      <div className="w-8/12 mx-auto">
        {/* Displaying */}
        {loading ? null : (
          <div className="w-full">
            <div className="w-full px-4 py-4">
              <div className="text-lg font-semibold underline underline-offset-1 mb-3 flex items-center justify-between">
                <p>Users</p>
                <p>Roles</p>
              </div>
              <div className="w-full space-y-5">
                {users &&
                  users.map((currValue, index) => {
                    return (
                      <div
                        key={currValue.id}
                        className="px-2 py-2 flex justify-between items-center border border-opacity-70 rounded-md  bg-neutral-200"
                      >
                        <div className="space-y-2">
                          <p>
                            Id:{" "}
                            <span className="font-semibold">
                              {currValue.id}
                            </span>
                          </p>
                          <p>
                            Username:{" "}
                            <span className="font-semibold">
                              {currValue.username}
                            </span>
                          </p>
                          <p>
                            Email:{" "}
                            <span className="font-semibold">
                              {currValue.email}
                            </span>
                          </p>
                        </div>
                        <div className="space-x-3">
                          <form className="w-full">
                            {roles.map((role) => {
                              return (
                                <div
                                  key={role.role_id}
                                  className="flex items-center"
                                >
                                  <input
                                    type="checkbox"
                                    id={role.role_name}
                                    name={role.role_name}
                                  />
                                  <label htmlFor={role.role_name}>
                                    {role.role_name}
                                  </label>
                                </div>
                              );
                            })}
                            <button
                              type="submit"
                              className="bg-blue-400 rounded-md px-2 py-1"
                            >
                              Save
                            </button>
                          </form>
                        </div>
                      </div>
                    );
                  })}
              </div>
            </div>
          </div>
        )}
      </div>
    </AdminLayout>
  );
};

export default ManageUserRoles;
