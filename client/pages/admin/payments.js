import AdminLayout from "../../components/AdminLayout";
import { useState, useEffect } from "react";
import axios from "../../utils/axios";

const Payments = () => {
  const [payments, setPayments] = useState([]);
  const [loading, setLoading] = useState(false);
  const [paymentAmount, setPaymentAmount] = useState("");
  const [paymentRemarks, setPaymentRemarks] = useState("");
  const [payerId, setPayerId] = useState("");

  useEffect(() => {
    fetchPayments();
  }, []);

  const fetchPayments = async () => {
    try {
      setLoading(true);
      const response = await axios.get("/payments");
      setPayments(response.data.data);
      setLoading(false);
    } catch (err) {
      setLoading(false);
      console.log(err.response.data);
    }
  };

  const createPayment = async (e) => {
    try {
      e.preventDefault();
      await axios.post("/payments", {
        payment_amount: paymentAmount,
        payment_remarks: paymentRemarks,
        user_id: payerId,
      });
      setPaymentAmount("");
      setPaymentRemarks("");
      setPayerId("");
      fetchPayments();
    } catch (err) {
      console.log(err.response.data);
    }
  };

  return (
    <AdminLayout>
      <div className="w-8/12 mx-auto">
        {/* Creating module */}
        <div className="mx-auto px-4 py-4">
          <div className="bg-white border-black border-2 rounded-lg border-opacity-40 px-10 py-16 ">
            <h4 className="font-semibold text-xl mb-6 text-center underline underline-offset-2">
              Make Payment
            </h4>
            <form
              className="w-full space-y-3"
              method="POST"
              onSubmit={(e) => createPayment(e)}
            >
              <div className="w-full flex items-center justify-between space-x-10">
                <label htmlFor="p_amount">Payment amount</label>
                <input
                  type="text"
                  id="p_amount"
                  name="p_amount"
                  placeholder="Enter payment amount."
                  className="border-2 border-black border-opacity-50 px-2 py-2 rounded-lg"
                  value={paymentAmount}
                  onChange={(e) => setModuleName(e.target.value)}
                />
              </div>
              <div className="w-full flex items-center justify-between space-x-10">
                <label htmlFor="p_remarks">Payment Remarks</label>
                <input
                  type="text"
                  id="p_remarks"
                  name="p_remarks"
                  placeholder="Enter payment remarks."
                  className="border-2 border-black border-opacity-50 px-2 py-2 rounded-lg"
                  value={paymentRemarks}
                  onChange={(e) => setPaymentRemarks(e.target.value)}
                />
              </div>
              <div className="w-full flex items-center justify-between space-x-10">
                <label htmlFor="u_id">Payer Id</label>
                <input
                  type="text"
                  id="u_id"
                  name="u_id"
                  placeholder="Enter payer's id"
                  className="border-2 border-black border-opacity-50 px-2 py-2 rounded-lg"
                  value={payerId}
                  onChange={(e) => setPayerId(e.target.value)}
                />
              </div>
              <div className="w-full flex items-center justify-center ">
                <button
                  type="submit"
                  className="mt-4 px-4 py-2 bg-blue-400 rounded-lg"
                >
                  Pay
                </button>
              </div>
            </form>
          </div>
        </div>

        {/* Displaying */}
        {loading ? null : (
          <div className="w-full">
            <div className="w-full px-4 py-4">
              <p className="text-lg font-semibold underline underline-offset-1 mb-3">
                Payments
              </p>
              <div className="w-full space-y-5">
                {payments &&
                  payments.map((currValue, index) => {
                    return (
                      <div
                        key={currValue.payment_id}
                        className="px-2 py-2 flex justify-between items-center border border-opacity-70 rounded-md  bg-neutral-200"
                      >
                        <div className="space-y-2">
                          <p>
                            Id:{" "}
                            <span className="font-semibold">
                              {currValue.payment_id}
                            </span>
                          </p>
                          <p>
                            Payment Amount:{" "}
                            <span className="font-semibold">
                              {currValue.payment_amount}
                            </span>
                          </p>
                          <p>
                            Payment Remarks:{" "}
                            <span className="font-semibold">
                              {currValue.payment_remarks}
                            </span>
                          </p>
                          <p>
                            Paid At:{" "}
                            <span className="font-semibold">
                              {currValue.createdAt}
                            </span>
                          </p>
                          <p>
                            Payment Processed By:{" "}
                            <span className="font-semibold">
                              {currValue.processed_by}
                            </span>
                          </p>
                          <p>
                            Paid By:{" "}
                            <span className="font-semibold">
                              {currValue.paid_by.username}
                            </span>
                          </p>
                          <p>
                            Payer Id:{" "}
                            <span className="font-semibold">
                              {currValue.paid_by.id}
                            </span>
                          </p>
                        </div>
                      </div>
                    );
                  })}
              </div>
            </div>
          </div>
        )}
      </div>
    </AdminLayout>
  );
};

export default Payments;
