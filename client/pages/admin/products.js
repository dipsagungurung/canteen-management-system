import AdminLayout from "../../components/AdminLayout";
import { useState, useEffect } from "react";
import axios from "../../utils/axios";

const Products = () => {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [productName, setProductName] = useState("");
  const [productDescription, setProductDescription] = useState("");
  const [productPrice, setProductPrice] = useState("");
  const [categoryId, setCategoryId] = useState("");

  useEffect(() => {
    fetchproducts();
  }, []);

  const fetchproducts = async () => {
    try {
      setLoading(true);
      const response = await axios.get("/products");
      setProducts(response.data.data);
      setLoading(false);
    } catch (err) {
      setLoading(false);
      console.log(err.response.data);
    }
  };

  const createProduct = async (e) => {
    try {
      e.preventDefault();
      await axios.post("/products", {
        product_name: productName,
        product_description: productDescription,
        product_price: productPrice,
        category_id: categoryId,
      });
      setProductName("");
      setProductDescription("");
      setProductPrice("");
      setCategoryId("");
      fetchproducts();
    } catch (err) {
      console.log(err.response.data);
    }
  };

  const editProduct = async (productId) => {};

  const deleteProduct = async (productId) => {
    try {
      await axios.delete(`/products/${productId}`);
      fetchproducts();
    } catch (err) {
      console.log(err.response.data);
    }
  };

  return (
    <AdminLayout>
      <div className="w-8/12 mx-auto">
        {/* Creating Product */}
        <div className="mx-auto px-4 py-4">
          <div className="bg-white border-black border-2 rounded-lg border-opacity-40 px-10 py-16 ">
            <h4 className="font-semibold text-xl mb-6 text-center underline underline-offset-2">
              Create Product
            </h4>
            <form
              className="w-full space-y-3"
              method="POST"
              onSubmit={(e) => createProduct(e)}
            >
              <div className="w-full flex items-center justify-between space-x-10">
                <label htmlFor="p_name">Product name</label>
                <input
                  type="text"
                  id="p_name"
                  name="p_name"
                  placeholder="Enter product name."
                  className="border-2 border-black border-opacity-50 px-2 py-2 rounded-lg"
                  value={productName}
                  onChange={(e) => setProductName(e.target.value)}
                />
              </div>
              <div className="w-full flex items-center justify-between space-x-10">
                <label htmlFor="p_description">Product Description</label>
                <input
                  type="text"
                  id="p_description"
                  name="p_description"
                  placeholder="Enter product description"
                  className="border-2 border-black border-opacity-50 px-2 py-2 rounded-lg"
                  value={productDescription}
                  onChange={(e) => setProductDescription(e.target.value)}
                />
              </div>
              <div className="w-full flex items-center justify-between space-x-10">
                <label htmlFor="p_price">Product Price</label>
                <input
                  type="text"
                  id="p_price"
                  name="p_price"
                  placeholder="Enter product price"
                  className="border-2 border-black border-opacity-50 px-2 py-2 rounded-lg"
                  value={productPrice}
                  onChange={(e) => setProductPrice(e.target.value)}
                />
              </div>
              <div className="w-full flex items-center justify-between space-x-10">
                <label htmlFor="c_id">Category Id</label>
                <input
                  type="text"
                  id="c_id"
                  name="c_id"
                  placeholder="Enter product price"
                  className="border-2 border-black border-opacity-50 px-2 py-2 rounded-lg"
                  value={categoryId}
                  onChange={(e) => setCategoryId(e.target.value)}
                />
              </div>
              <div className="w-full flex items-center justify-center ">
                <button
                  type="submit"
                  className="mt-4 px-4 py-2 bg-blue-400 rounded-lg"
                >
                  Create
                </button>
              </div>
            </form>
          </div>
        </div>

        {/* Displaying */}
        {loading ? null : (
          <div className="w-full">
            <div className="w-full px-4 py-4">
              <p className="text-lg font-semibold underline underline-offset-1 mb-3">
                Products
              </p>
              <div className="w-full space-y-5">
                {products &&
                  products.map((currValue, index) => {
                    return (
                      <div
                        key={currValue.product_id}
                        className="px-2 py-2 flex justify-between items-center border border-opacity-70 rounded-md  bg-neutral-200"
                      >
                        <div className="space-y-2">
                          <p>
                            Id:{" "}
                            <span className="font-semibold">
                              {currValue.product_id}
                            </span>
                          </p>
                          <p>
                            Product name:{" "}
                            <span className="font-semibold">
                              {currValue.product_name}
                            </span>
                          </p>
                          <p>
                            Product price:{" "}
                            <span className="font-semibold">
                              {currValue.product_price}
                            </span>
                          </p>
                          <p>
                            Category Id:{" "}
                            <span className="font-semibold">
                              {currValue.category_id}
                            </span>
                          </p>
                        </div>
                        <div className="space-x-3">
                          <button
                            type="button"
                            className="bg-blue-400 rounded-md px-2 py-1"
                            onClick={() => editProduct(currValue.product_id)}
                          >
                            View
                          </button>
                          <button
                            type="button"
                            className="bg-blue-400 rounded-md px-2 py-1"
                            onClick={() => editProduct(currValue.product_id)}
                          >
                            Edit
                          </button>
                          <button
                            type="button"
                            className="bg-red-400 rounded-md px-2 py-1"
                            onClick={() => deleteProduct(currValue.product_id)}
                          >
                            Delete
                          </button>
                        </div>
                      </div>
                    );
                  })}
              </div>
            </div>
          </div>
        )}
      </div>
    </AdminLayout>
  );
};

export default Products;
