import AdminLayout from "../../components/AdminLayout";
import { useState, useEffect } from "react";
import axios from "../../utils/axios";

const Roles = () => {
  const [roles, setRoles] = useState([]);
  const [loading, setLoading] = useState(false);
  const [roleName, setRoleName] = useState("");

  useEffect(() => {
    fetchRoles();
  }, []);

  const fetchRoles = async () => {
    try {
      setLoading(true);
      const response = await axios.get("/roles");
      setRoles(response.data.data);
      setLoading(false);
    } catch (err) {
      setLoading(false);
      console.log(err.response.data);
    }
  };

  const createRole = async (e) => {
    try {
      e.preventDefault();
      await axios.post("/roles", {
        role_name: roleName,
      });
      setRoleName("");
      fetchRoles();
    } catch (err) {
      console.log(err.response.data);
    }
  };

  const deleteRole = async (roleId) => {
    try {
      await axios.delete(`/roles/${roleId}`);
      fetchRoles();
    } catch (err) {
      console.log(err.response.data);
    }
  };

  return (
    <AdminLayout>
      <div className="w-8/12 mx-auto">
        {/* Creating role */}
        <div className="mx-auto px-4 py-4">
          <div className="bg-white border-black border-2 rounded-lg border-opacity-40 px-10 py-16 ">
            <h4 className="font-semibold text-xl mb-6 text-center underline underline-offset-2">
              Create Role
            </h4>
            <form
              className="w-full space-y-3"
              method="POST"
              onSubmit={(e) => createRole(e)}
            >
              <div className="w-full flex items-center justify-between space-x-10">
                <label htmlFor="r_name">Role name</label>
                <input
                  type="text"
                  id="r_name"
                  name="r_name"
                  placeholder="Enter role name."
                  className="border-2 border-black border-opacity-50 px-2 py-2 rounded-lg"
                  value={roleName}
                  onChange={(e) => setRoleName(e.target.value)}
                />
              </div>
              <div className="w-full flex items-center justify-center ">
                <button
                  type="submit"
                  className="mt-4 px-4 py-2 bg-blue-400 rounded-lg"
                >
                  Create
                </button>
              </div>
            </form>
          </div>
        </div>

        {/* Displaying */}
        {loading ? null : (
          <div className="w-full">
            <div className="w-full px-4 py-4">
              <p className="text-lg font-semibold underline underline-offset-1 mb-3">
                Roles
              </p>
              <div className="w-full space-y-5">
                {roles &&
                  roles.map((currValue, index) => {
                    return (
                      <div
                        key={currValue.role_id}
                        className="px-2 py-2 flex justify-between items-center border border-opacity-70 rounded-md  bg-neutral-200"
                      >
                        <p>
                          Id:{" "}
                          <span className="font-semibold">
                            {currValue.role_id}
                          </span>
                        </p>
                        <p>
                          Role name:{" "}
                          <span className="font-semibold">
                            {currValue.role_name}
                          </span>
                        </p>
                        <button
                          type="button"
                          className="bg-red-400 rounded-md px-2 py-1"
                          onClick={() => deleteRole(currValue.role_id)}
                        >
                          Delete
                        </button>
                      </div>
                    );
                  })}
              </div>
            </div>
          </div>
        )}
      </div>
    </AdminLayout>
  );
};

export default Roles;
