import { useEffect, useContext } from "react";
import axios from "../utils/axios";
import { UserContext } from "./_app";
import HeroSection from "../components/HeroSection";
import Layout from "../components/Layout";

export default function Home() {
  const [, setUserData] = useContext(UserContext);
  useEffect(() => {
    fetchUserData();
  }, []);

  const fetchUserData = async () => {
    try {
      const TOKEN = localStorage.getItem("jwt-token");
      if (TOKEN) {
        const BEARER_TOKEN = "Bearer " + TOKEN;
        axios.defaults.headers.common["Authorization"] = BEARER_TOKEN;
      }
      const response = await axios.get("/profile");
      console.log(response.data.data);
      setUserData(response.data.data);
    } catch (err) {
      console.log(err.response);
    }
  };
  return (
    <Layout>
      <HeroSection />
    </Layout>
  );
}
