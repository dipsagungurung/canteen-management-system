import { useEffect, useState } from "react";
import axios from "../utils/axios";
import Layout from "../components/Layout";

const Orders = () => {
  const [orders, setOrders] = useState();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    fetchOrders();
  }, []);

  const fetchOrders = async () => {
    try {
      setLoading(true);
      const TOKEN = localStorage.getItem("jwt-token");
      if (TOKEN) {
        const BEARER_TOKEN = "Bearer " + TOKEN;
        axios.defaults.headers.common["Authorization"] = BEARER_TOKEN;
      }
      const response = await axios.get("/orders");
      console.log(response.data);
    } catch (err) {
      setLoading(false);
      console.log(err.response.data.data);
    }
  };
  return (
    <Layout>
      <div className="w-full pt-6 ">Order section</div>
    </Layout>
  );
};

export default Orders;
