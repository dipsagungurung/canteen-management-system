import { useState } from "react";
import { useRouter } from "next/router";
import axios from "../utils/axios";

const SignUp = () => {
  const router = useRouter();
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [repeatPassword, setRepeatPassword] = useState("");
  const [loading, setLoading] = useState(false);

  const onSubmitHandler = async (e) => {
    try {
      e.preventDefault();
      setLoading(true);
      const response = await axios.post("/register", {
        username: username,
        email: email,
        password: password,
        repeat_password: repeatPassword,
      });
      setLoading(false);
      router.push("/signIn");

      console.log(response.data);
    } catch (err) {
      setLoading(false);
      console.log(err.response.data);
    }
  };

  return (
    <div className="max-w-7xl mx-auto h-screen flex items-center justify-center">
      <div className="w-full flex items-center justify-center">
        <div className="bg-white border-black border-2 rounded-lg border-opacity-40 px-10 py-16 ">
          <h4 className="font-semibold text-xl mb-4">Register</h4>
          <form
            className="w-full space-y-3"
            method="POST"
            onSubmit={(e) => onSubmitHandler(e)}
          >
            <div className="w-full flex items-center justify-between space-x-10">
              <label htmlFor="username">Username</label>
              <input
                type="text"
                id="username"
                name="username"
                placeholder="Enter your username."
                className="border-2 border-black border-opacity-50 px-2 py-2 rounded-lg"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
              />
            </div>
            <div className="w-full flex items-center justify-between space-x-10">
              <label htmlFor="email">Email</label>
              <input
                type="email"
                id="email"
                name="email"
                placeholder="Enter your email."
                className="border-2 border-black border-opacity-50 px-2 py-2 rounded-lg"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>
            <div className="w-full flex items-center justify-between space-x-10">
              <label htmlFor="password">Password</label>
              <input
                type="password"
                id="password"
                name="password"
                placeholder="Enter your password."
                className="border-2 border-black border-opacity-50 px-2 py-2 rounded-lg"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </div>
            <div className="w-full flex items-center justify-between space-x-10">
              <label htmlFor="r_password">Confirm Password</label>
              <input
                type="password"
                id="r_password"
                name="r_password"
                placeholder="Confirm password"
                className="border-2 border-black border-opacity-50 px-2 py-2 rounded-lg"
                value={repeatPassword}
                onChange={(e) => setRepeatPassword(e.target.value)}
              />
            </div>
            <div className="w-full flex items-center justify-center ">
              <button
                type="submit"
                className="mt-4 px-4 py-2 bg-blue-400 rounded-lg"
              >
                {loading ? "Processing" : "Sumbit"}
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default SignUp;
