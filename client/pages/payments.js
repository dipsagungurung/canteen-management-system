import Layout from "../components/Layout";
import { useContext, useEffect } from "react";
import { UserContext } from "./_app";
import { useRouter } from "next/router";

const Payments = () => {
  const [user] = useContext(UserContext);
  const router = useRouter();

  useEffect(() => {
    if (!user) {
      router.push("/");
    }
  }, [router, user]);

  let paymentBody;
  paymentBody = (
    <div className="w-full min-h-screen flex items-center justify-center">
      <p className="text-lg text-opacity-70 font-semibold">
        No Payments made yet.
      </p>
    </div>
  );

  if (user && user.payments.length > 0) {
    paymentBody = (
      <div className="w-full px-6 py-5">
        <h4 className="font-semibold text-lg text-opacity-70 mb-5">
          Payment History
        </h4>
        {user.payments.map((currentValue, index) => {
          return (
            <div
              key={index}
              className="px-2 py-2 rounded-md border-2 border-opacity-50 flex items-center justify-between"
            >
              <p>
                Payment Remarks:{" "}
                <span className="font-semibold">
                  {currentValue.payment_remarks}
                </span>
              </p>
              <div className="flex flex-col">
                <p>
                  Payment Amount:{" "}
                  <span className="font-semibold">
                    Rs.{currentValue.payment_amount}
                  </span>
                </p>
                <p>
                  Date:{" "}
                  <span className="font-semibold">
                    {currentValue.createdAt}
                  </span>
                </p>
              </div>
            </div>
          );
        })}
      </div>
    );
  }
  return (
    <Layout>
      <div className="w-full pt-6 ">{paymentBody}</div>
    </Layout>
  );
};

export default Payments;
