import Layout from "../components/Layout";
import { useContext } from "react";
import { UserContext } from "./_app";

const Account = () => {
  const [userData] = useContext(UserContext);

  let accoutBody;

  accoutBody = (
    <div className="w-full pt-6 ">
      <h4 className="font-semibold text-lg text-opacity-70 mb-5">My Account</h4>
      <div>
        <p>Sorry! Could not find the account</p>
      </div>
    </div>
  );

  if (userData) {
    accoutBody = (
      <div className="w-full pt-6 ">
        <h4 className="font-semibold text-lg text-opacity-70 mb-5">
          My Account
        </h4>
        <div>
          <p>
            Advance balance:{" "}
            <span className="font-semibold">
              {userData.account.credit_balance < 0
                ? -userData.account.credit_balance
                : 0}{" "}
            </span>
          </p>
          <p>
            Credit balance:{" "}
            <span className="font-semibold">
              {userData.account.credit_balance > 0
                ? userData.account.credit_balance
                : 0}{" "}
            </span>
          </p>
        </div>
      </div>
    );
  }

  return <Layout>{accoutBody}</Layout>;
};

export default Account;
