const {
  User,
  Role,
  Product,
  Order,
  Payment,
  OrderItem,
  UserRole,
  Account,
} = require("../models");
const { NotFoundException } = require("../exceptions/notFoundException");

class ProfileRepository {
  async getProfile(userId) {
    const user = await User.findOne({
      where: {
        id: userId,
      },
      include: [
        {
          model: Role,
          through: {
            model: UserRole,
            attributes: [],
          },
        },
        {
          model: Payment,
          as: "payments",
        },
        {
          model: Order,
          as: "orders",
          include: [
            {
              model: OrderItem,
              as: "items",
              include: [
                {
                  model: Product,
                  attributes: { exclude: ["product_price"] },
                },
              ],
            },
          ],
        },
        {
          model: Account,
        },
      ],
    });

    if (user == null) throw new NotFoundException("Profile not found");

    return user;
  }
}

module.exports = new ProfileRepository();
