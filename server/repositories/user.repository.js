const argon2 = require("argon2");

const {
  User,
  Account,
  Role,
  UserRole,
  Payment,
  Order,
  OrderItem,
  Product,
} = require("../models");
const { NotFoundException } = require("../exceptions/notFoundException");

class UserRepository {
  async getAllUsers() {
    const users = await User.findAll({
      include: [
        {
          model: Role,
          order: [["role_id"]],
          through: {
            model: UserRole,
            attributes: [],
          },
        },
      ],
    });

    if (users.length == 0) throw new NotFoundException();

    return users;
  }

  async getOneUser(userId) {
    const user = await User.findOne({
      where: {
        id: userId,
      },
      include: [
        {
          model: Role,
          through: {
            model: UserRole,
            attributes: [],
          },
        },
        {
          model: Payment,
          as: "payments",
        },
        {
          model: Order,
          as: "orders",
          include: [
            {
              model: OrderItem,
              as: "items",
              include: [
                {
                  model: Product,
                  attributes: { exclude: ["product_price"] },
                },
              ],
            },
          ],
        },
      ],
    });

    if (user == null) throw new NotFoundException("User not found");

    return user;
  }

  async hashUserPassword(password) {
    const hashedPassword = await argon2.hash(password, {
      type: argon2.argon2id,
    });
    return hashedPassword;
  }

  async createUser(userData) {
    const user = await User.create(userData);
    return user;
  }

  async createUserAccount(userId) {
    await Account.create({
      user_id: userId,
    });
  }
}

module.exports = new UserRepository();
