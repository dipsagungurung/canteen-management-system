const { meilisearchClient } = require("../configs/meilisearch.config");

class SearchRepository {
  async search(q) {
    const result = await meilisearchClient.index("products").search(q);
    return result;
  }
}

module.exports = new SearchRepository();
