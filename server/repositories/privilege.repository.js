const { NotFoundException } = require("../exceptions/notFoundException");
const { Module, ModulePrivilege } = require("../models");

class PrivilegeRepository {
  async createModuleMappings(transaction, mappingData) {
    await ModulePrivilege.bulkCreate(mappingData, { transaction: transaction });
  }

  async destroyModuleMappings(transaction, moduleId) {
    const module = await Module.findOne(
      {
        where: {
          module_id: moduleId,
        },
      },
      { transaction: transaction }
    );

    if (module === null) {
      throw new NotFoundException("Module doesnot exist");
    }

    await ModulePrivilege.destroy(
      {
        where: {
          module_id: moduleId,
        },
      },
      { transaction: transaction }
    );
  }
}

module.exports = new PrivilegeRepository();
