const { Category } = require("../models");
const { NotFoundException } = require("../exceptions/notFoundException");
const { ValidationException } = require("../exceptions/validationException");
const { UniqueConstraintError } = require("sequelize");

class CategoryRepository {
  async getAllCategoriesAsTree() {
    try {
      const categories = await Category.findAll({
        where: {
          parent_id: null,
        },
        attributes: {
          exclude: ["createdAt", "updatedAt"],
        },
        include: [
          {
            model: Category,
            as: "children",

            attributes: {
              exclude: ["createdAt", "updatedAt"],
            },
          },
        ],
      });

      if (categories.length == 0) throw new NotFoundException();

      return categories;
    } catch (err) {
      throw err;
    }
  }

  async getAllCategoriesAsList() {
    try {
      const categories = await Category.findAll({
        attributes: {
          exclude: ["createdAt", "updatedAt"],
        },
      });
      if (categories.length == 0) throw new NotFoundException();

      return categories;
    } catch (err) {
      throw err;
    }
  }

  async getOneCategory(categoryId) {
    try {
      const category = await Category.findOne({
        where: {
          category_id: categoryId,
        },
      });
      if (category == null)
        throw new NotFoundException(
          `Category of given id: ${categoryId} doesnot exist`
        );

      return category;
    } catch (err) {
      throw err;
    }
  }

  async createCategory(categoryData) {
    try {
      await Category.create(categoryData);
    } catch (err) {
      if (err instanceof UniqueConstraintError) {
        throw new ValidationException(err.message);
      }
      throw err;
    }
  }

  async updateCategory(category, updateData) {
    try {
      await category.update(updateData);
    } catch (err) {
      if (err instanceof UniqueConstraintError) {
        throw new ValidationException(err.message);
      }
      throw err;
    }
  }

  async deleteCategory(categoryId) {
    try {
      const category = await this.getOneCategory(categoryId);
      await category.destroy();
    } catch (err) {
      throw err;
    }
  }
}

module.exports = new CategoryRepository();
