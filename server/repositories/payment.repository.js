const { Op } = require("sequelize");

const { NotFoundException } = require("../exceptions/notFoundException");
const { Payment, Account, User } = require("../models");

class PaymentRepository {
  async createPayment(transaction, processedByUser, paymentData) {
    try {
      await Payment.create(
        {
          ...paymentData,
          processed_by: processedByUser.id,
        },
        {
          transaction: transaction,
        }
      );
    } catch (err) {
      throw err;
    }
  }

  async deductCreditFromUserAccount(transaction, paymentData) {
    try {
      const account = await Account.findOne(
        {
          where: {
            user_id: paymentData.user_id,
          },
        },
        {
          transaction: transaction,
        }
      );
      if (account === null) {
        throw new NotFoundException("User account not found");
      }

      // deduct `credit_balance`
      account.credit_balance =
        account.credit_balance - paymentData.payment_amount;

      await account.save({ transaction: transaction });
    } catch (err) {
      throw err;
    }
  }

  async getAllPayments() {
    try {
      const payments = await Payment.findAll({
        order: [["createdAt", "DESC"]],
        include: [{ model: User, as: "paid_by" }],
      });
      return payments;
    } catch (err) {
      throw err;
    }
  }

  async getTodaysPayments() {
    const TODAY_START = new Date().setHours(0, 0, 0, 0);
    const NOW = new Date();

    const payments = await Payment.findAll({
      where: {
        createdAt: {
          [Op.gt]: TODAY_START,
          [Op.lt]: NOW,
        },
      },
      order: [["createdAt", "DESC"]],
    });

    return payments;
  }

  async getUserPayments(userId) {
    try {
      const payments = await Payment.findAll({
        where: {
          user_id: userId,
        },
        order: [["createdAt", "DESC"]],
      });
      return payments;
    } catch (err) {
      throw err;
    }
  }

  async getOneUser(userId) {
    const user = await User.findOne({
      where: {
        id: userId,
      },
    });

    if (user === null) {
      throw new NotFoundException(`User of id ${userId} not found`);
    }

    return user;
  }
}

module.exports = new PaymentRepository();
