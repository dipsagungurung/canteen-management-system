const streamifier = require("streamifier");
const { UniqueConstraintError, Op } = require("sequelize");

const { meilisearchClient } = require("../configs/meilisearch.config");
const { cloudinary } = require("../configs/cloudinary.config");
const { NotFoundException } = require("../exceptions/notFoundException");
const { ValidationException } = require("../exceptions/validationException");
const { HttpException } = require("../exceptions/httpException");
const { Product, Category } = require("../models");
const { ProductImage } = require("../models");

class ProductRepository {
  async createProduct(transaction, productData) {
    try {
      const product = await Product.create(productData, {
        transaction: transaction,
      });
      return product;
    } catch (err) {
      if (err instanceof UniqueConstraintError) {
        throw new ValidationException(err.message);
      }
      throw err;
    }
  }

  async uploadToCloudinary(files) {
    const uploadResult = await Promise.all(
      files.map((file) => {
        return new Promise((resolve, reject) => {
          let cld_upload_stream = cloudinary.uploader.upload_stream(
            {
              folder: "Canteen_Management_System",
            },
            function (error, result) {
              if (error) {
                reject(error);
              }
              if (result) {
                resolve(result);
              }
            }
          );
          streamifier.createReadStream(file.buffer).pipe(cld_upload_stream);
        });
      })
    );

    return uploadResult;
  }

  async storeProductImagesMetadata(transaction, productId, uploadResult) {
    if (uploadResult.length === 0) {
      return;
    } else {
      const mappingData = uploadResult.map((item) => {
        return {
          product_id: productId,
          product_image_url: item.secure_url,
          public_id: item.public_id,
        };
      });

      await ProductImage.bulkCreate(mappingData, { transaction: transaction });
    }
  }

  async getAllProducts() {
    const products = await Product.findAll({
      include: [
        {
          model: ProductImage,
          as: "product_images",
        },
      ],
    });
    if (products.length === 0) {
      throw new NotFoundException("No products");
    }
    return products;
  }

  async getOneProduct(productId) {
    const product = await Product.findOne({
      where: {
        product_id: productId,
      },
      include: [
        {
          model: ProductImage,
          as: "product_images",
        },
      ],
    });
    if (product === null || product === undefined) {
      throw new NotFoundException(`Product of id: ${productId} not found`);
    }

    return product;
  }

  async updateProduct(productId, productData) {
    const product = await Product.findOne({
      where: {
        product_id: productId,
      },
    });
    if (product === null || product === undefined) {
      throw new NotFoundException(`Product of id: ${productId} not found`);
    }
    await product.update(productData);
    return product;
  }

  async deleteProduct(transaction, productId) {
    const product = await this.getOneProduct(productId);
    await product.destroy({ transaction: transaction });
  }

  async deleteImagesFromCloudinary(productId) {
    const result = await ProductImage.findAll({
      where: {
        [Op.or]: [{ product_id: productId }, { product_id: null }],
      },
      attributes: ["public_id"],
    });
    if (result.length === 0) {
      return;
    }

    const publicIds = result.map((image) => {
      return image.public_id;
    });
    await cloudinary.api.delete_resources(publicIds, function (error, result) {
      if (error) {
        console.log(error);
        throw new HttpException(500, `Cannot delete the images`);
      }
    });
  }

  async deleteImagesFromDB(transaction, productId) {
    await ProductImage.destroy(
      {
        where: {
          [Op.or]: [{ product_id: productId }, { product_id: null }],
        },
      },
      { transaction: transaction }
    );
  }

  // fetch products from a category
  // upto depth of 2
  async getAllProductsOfCategory(category) {
    let categoryAndProductTree;
    let products;

    // fetching products from the parent category
    if (category.parent_id == null) {
      categoryAndProductTree = await Category.findOne({
        where: {
          category_id: category.category_id,
        },
        nest: true,
        attributes: ["category_id"],
        include: [
          {
            model: Category,
            as: "children",
            attributes: ["category_id"],
            include: [
              {
                model: Product,
                as: "products",
                attributes: {
                  exclude: ["createdAt", "updatedAt"],
                },
                include: [
                  {
                    model: ProductImage,
                    as: "product_images",
                  },
                ],
              },
            ],
          },
        ],
      });
      if (categoryAndProductTree == null) throw new NotFoundException();
      // flatten the tree to get products only
      products = this.flattenCategoryProductTree(
        categoryAndProductTree.get({ plain: true })
      );
    }

    // fetching products from child category
    if (category.parent_id != null) {
      categoryAndProductTree = await Category.findOne({
        where: {
          category_id: category.category_id,
        },
        attributes: ["category_id"],
        include: [
          {
            model: Product,
            as: "products",
            attributes: {
              exclude: ["createdAt", "updatedAt"],
            },
            include: [
              {
                model: ProductImage,
                as: "product_images",
              },
            ],
          },
        ],
      });
      if (categoryAndProductTree == null) throw new NotFoundException();
      // flatten the tree to get products
      products = this.flattenCategoryProductTree(
        categoryAndProductTree.get({ plain: true })
      );
    }

    if (products.length == 0) throw new NotFoundException();

    return products;
  }

  // get products only from the categoryproduct tree
  flattenCategoryProductTree(tree) {
    let result;
    const getProducts = (fTree) => {
      let products = [];
      if (fTree.products && fTree.products.length > 0) {
        fTree.products.forEach((product) => {
          products.push(product);
        });
      }
      if (fTree.children && fTree.children.length > 0) {
        fTree.children.forEach((child) => {
          products = [...products, ...getProducts(child)];
        });
      }

      return products;
    };

    result = getProducts(tree);

    return result;
  }

  async deleteSingleImageOfProduct(productId, publicId) {
    await this.getOneProduct(productId);
    const productImage = await ProductImage.findOne({
      where: {
        product_id: productId,
        public_id: publicId,
      },
    });

    if (productImage === null) {
      throw new NotFoundException("Image with the given publicId not found");
    }

    await productImage.destroy();
    await cloudinary.uploader.destroy(publicId, function (error, result) {
      if (error) {
        console.log(`Error when deleting image from cloud: ${error}`);
      }
    });
  }

  async addProductToSearchEngine(product) {
    await meilisearchClient.index("products").addDocuments([product]);
  }

  async updateProductFromSearchEngine(product) {
    await meilisearchClient.index("products").updateDocuments([product]);
  }

  async deleteProductFromSearchEngine(productId) {
    await meilisearchClient.index("products").deleteDocument(productId);
  }
}

module.exports = new ProductRepository();
