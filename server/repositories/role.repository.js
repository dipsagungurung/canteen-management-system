const { NotFoundException } = require("../exceptions/notFoundException");
const { User, UserRole, Role, Access } = require("../models");

class RoleRepository {
  async assignUserRoles(transaction, mappingData) {
    await UserRole.bulkCreate(mappingData, { transaction: transaction });
  }

  async deleteUserRoles(transaction, userId) {
    const user = await User.findOne({
      where: {
        id: userId,
      },
    });

    if (user === null) {
      throw new NotFoundException("User doesnot exist");
    }

    await UserRole.destroy(
      {
        where: {
          user_id: userId,
        },
      },
      { transaction: transaction }
    );
  }

  async addAccessToRole(transaction, mappingData) {
    await Access.bulkCreate(mappingData, { transaction: transaction });
  }

  async removeAccessFromRole(transaction, roleId) {
    const role = await Role.findOne({
      where: {
        role_id: roleId,
      },
    });
    if (role === null) {
      throw new NotFoundException("Role doesnot exist");
    }

    await Access.destroy(
      {
        where: {
          role_id: roleId,
        },
      },
      { transaction: transaction }
    );
  }
}

module.exports = new RoleRepository();
