const { QueryTypes } = require("sequelize");

const { sequelize } = require("../configs/database.config");
const { Order, Account, OrderItem, Product } = require("../models");
const productRepository = require("../repositories/product.repository");

class OrderRepository {
  async getAllOrders() {
    const orders = await Order.findAll({
      order: [["createdAt", "DESC"]],
    });
    return orders;
  }

  async getAllTodaysOrders() {
    const TODAY_START = new Date().setHours(0, 0, 0, 0);
    const NOW = new Date();

    const orders = await Order.findAll({
      where: {
        createdAt: {
          [Op.gt]: TODAY_START,
          [Op.lt]: NOW,
        },
      },
      order: [["createdAt", "DESC"]],
    });

    return orders;
  }

  async getAllUserOrders(userId) {
    try {
      const orders = await Order.findAll({
        where: {
          user_id: userId,
        },
        order: [["createdAt", "DESC"]],
      });
      return orders;
    } catch (err) {
      throw err;
    }
  }

  async getOneOrder(orderId) {
    const order = await Order.findOne({
      where: {
        order_id: orderId,
      },
      include: [
        {
          model: OrderItem,
          as: "items",
          include: [
            {
              model: Product,
              attributes: { exclude: ["product_price"] },
            },
          ],
        },
      ],
      raw: true,
      nest: true,
    });
    if (order === null) {
      throw new NotFoundException(`Order of id: ${orderId} doesnot exist`);
    }
    const totalPrice = await this.computeTotalPriceOfOrder(order.order_id);
    order.totalPrice = totalPrice;
    return order;
  }

  async getOneUserOrder(userId, orderId) {
    const order = await Order.findOne({
      where: {
        user_id: userId,
        order_id: orderId,
      },
      include: [
        {
          model: OrderItem,
          attributes: {
            exclude: ["processed_by"],
          },
          include: [
            {
              model: Product,
              attributes: { exclude: ["product_price"] },
            },
          ],
        },
      ],
      raw: true,
      nest: true,
    });
    if (order === null) {
      throw new NotFoundException(`Cannot find your order of id: ${orderId}`);
    }
    const totalPrice = await this.computeTotalPriceOfOrder(order.order_id);
    order.totalPrice = totalPrice;
    return order;
  }

  async createOrder(transaction, userId) {
    const order = await Order.create(
      {
        user_id: userId,
      },
      { transaction: transaction }
    );

    return order;
  }

  async addItemsToOrder(transaction, order, orderData) {
    const processedOrderData = await Promise.all(
      orderData.items.map(async (item) => {
        const product = await productRepository.getOneProduct(item.product_id);
        return {
          order_id: order.order_id,
          ...item,
          product_price: product.product_price,
        };
      })
    );
    /**
     * processedOrderData: [
     *  {
     *    order_id: orderId,
     *    product_id: productId,
     *    product_quantity: productQuantity,
     *    product_price: price of product at the time of buying
     *  },
     * ]
     */

    await OrderItem.bulkCreate(processedOrderData, {
      transaction: transaction,
    });
  }

  async creditUserAccount(transaction, userId, totalPrice) {
    const account = await Account.findOne({ where: { user_id: userId } });
    account.credit_balance = account.credit_balance + totalPrice;
    await account.save({ transaction: transaction });
  }

  async computeTotalPriceOfOrder(orderId) {
    const result = await sequelize.query(
      "SELECT SUM(product_quantity * product_price) AS totalprice FROM order_item WHERE order_id = ?",
      {
        replacements: [orderId],
        type: QueryTypes.SELECT,
      }
    );

    console.log(`
    ===========================
    ${result["0"].totalprice}
    ===========================
    `);
    return result[0].totalprice;
  }

  async updateOrder(transaction, processedByUser, orderId, orderUpdateData) {
    orderUpdateData.processedBy = processedByUser.id;
    const order = await Order.findOne({
      where: {
        order_id: orderId,
      },
    });
    await order.update(orderUpdateData, { transaction: transaction });
  }

  async deleteOrder(order) {
    await order.destroy();
  }
}

module.exports = new OrderRepository();
