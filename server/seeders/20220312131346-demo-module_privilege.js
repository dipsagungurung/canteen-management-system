"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "module_privilege",
      [
        {
          module_id: 1, // category
          privilege_id: 1, // create
        },
        {
          module_id: 1, // category
          privilege_id: 2, // read
        },
        {
          module_id: 1, // category
          privilege_id: 3, // update
        },
        {
          module_id: 1, // category
          privilege_id: 4, // delete
        },
        {
          module_id: 2, // product
          privilege_id: 1, // create
        },
        {
          module_id: 2, // product
          privilege_id: 2, // read
        },
        {
          module_id: 2, // product
          privilege_id: 3, // update
        },
        {
          module_id: 2, // product
          privilege_id: 4, // delete
        },
        {
          module_id: 3, // user
          privilege_id: 1, // create
        },
        {
          module_id: 3, // user
          privilege_id: 2, // read
        },
        {
          module_id: 3, // user
          privilege_id: 3, // update
        },
        {
          module_id: 3, // user
          privilege_id: 4, // delete
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("module_privilege", null, {});
  },
};
