"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "access",
      [
        {
          role_id: 1, // admin
          module_privilege_id: 1, // to create category
        },
        {
          role_id: 1, // admin
          module_privilege_id: 2, // to read category
        },
        {
          role_id: 1, // admin
          module_privilege_id: 3, // to update category
        },
        {
          role_id: 1, // admin
          module_privilege_id: 4, // to delete category
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("access", null, {});
  },
};
