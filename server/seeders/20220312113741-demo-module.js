"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "modules",
      [
        {
          module_name: "Category",
          module_endpoint: "/categories",
        },
        {
          module_name: "Product",
          module_endpoint: "/products",
        },
        {
          module_name: "User",
          module_endpoint: "/users",
        },
        {
          module_name: "Role",
          module_endpoint: "/roles",
        },
        {
          module_name: "Module",
          module_endpoint: "/modules",
        },
        {
          module_name: "Privilege",
          module_endpoint: "/privileges",
        },
        {
          module_name: "Manage Module Privileges",
          module_endpoint: "/privileges/manageModulePrivileges",
        },
        {
          module_name: "Manage User Roles",
          module_endpoint: "/roles/manageUserRoles",
        },
        {
          module_name: "Manage Role Access",
          module_endpoint: "/roles/manageRoleAccess",
        },
        {
          module_name: "Order",
          module_endpoint: "/orders",
        },
        {
          module_name: "Payments",
          module_endpoint: "/payments",
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("modules", null, {});
  },
};
