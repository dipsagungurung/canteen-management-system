"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert("users", [
      {
        username: "Super User",
        email: "super@abc.com",
        password:
          "$argon2id$v=19$m=4096,t=3,p=1$XHw2Ytk20cX0RKc4acv4Tg$zP9/1pR35mV16h6Tp8Rg/j1FySrit6zu/lfPC4oT5jQ", // hashed form of `abc`
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        username: "Ram Bahadur",
        email: "ram@abc.com",
        password:
          "$argon2id$v=19$m=4096,t=3,p=1$XHw2Ytk20cX0RKc4acv4Tg$zP9/1pR35mV16h6Tp8Rg/j1FySrit6zu/lfPC4oT5jQ", // hashed form of `abc`
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        username: "Shaym Bahadur",
        email: "shyam@abc.com",
        password:
          "$argon2id$v=19$m=4096,t=3,p=1$XHw2Ytk20cX0RKc4acv4Tg$zP9/1pR35mV16h6Tp8Rg/j1FySrit6zu/lfPC4oT5jQ", // hashed form of `abc`
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        username: "Hari Bahadur",
        email: "hari@abc.com",
        password:
          "$argon2id$v=19$m=4096,t=3,p=1$XHw2Ytk20cX0RKc4acv4Tg$zP9/1pR35mV16h6Tp8Rg/j1FySrit6zu/lfPC4oT5jQ", // hashed form of `abc`
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        username: "Kratos",
        email: "kratos@abc.com",
        password:
          "$argon2id$v=19$m=4096,t=3,p=1$XHw2Ytk20cX0RKc4acv4Tg$zP9/1pR35mV16h6Tp8Rg/j1FySrit6zu/lfPC4oT5jQ", // hashed form of `abc`
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("users", null, {});
  },
};
