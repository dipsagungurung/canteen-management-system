"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "categories",
      [
        {
          category_name: "Drinks",
          category_description: "Drinks and Liquors",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          category_name: "Breakfast",
          category_description: "Light foods for morning",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          category_name: "Snacks",
          category_description: "Light foods for day",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          category_name: "Hot Drinks",
          category_description: "Hot drinks",
          parent_id: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          category_name: "Cold Drinks",
          category_description: "Cold drinks",
          parent_id: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          category_name: "Non-veg snacks",
          category_description: "Snacks for non-vegeterian",
          parent_id: 3,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          category_name: "Veg snacks",
          category_description: "Snacks for vegeterian",
          parent_id: 7,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("categories", null, {});
  },
};
