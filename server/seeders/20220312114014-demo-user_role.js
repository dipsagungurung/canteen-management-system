"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "user_role",
      [
        {
          user_id: 1, // super user
          role_id: 1, // admin
        },
        {
          user_id: 2, // ram
          role_id: 2, // manager
        },
        {
          user_id: 3, // shyam
          role_id: 2, // manager
        },
        {
          user_id: 4, // hari
          role_id: 3, // staff
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
