"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "products",
      [
        // {
        //   product_name: "Sprite",
        //   product_description: "Sprite",
        //   product_price: 50,
        //   category_id: 5,
        //   createdAt: new Date(),
        //   updatedAt: new Date(),
        // },
        // {
        //   product_name: "Coca cola",
        //   product_description: "Coca-cola",
        //   product_price: 50,
        //   category_id: 5,
        //   createdAt: new Date(),
        //   updatedAt: new Date(),
        // },
        // {
        //   product_name: "Milk Tea",
        //   product_description: "Tea made with milk",
        //   product_price: 30,
        //   category_id: 4,
        //   createdAt: new Date(),
        //   updatedAt: new Date(),
        // },
        // {
        //   product_name: "Black Tea",
        //   product_description: "Tea made with tea leaves",
        //   product_price: 25,
        //   category_id: 4,
        //   createdAt: new Date(),
        //   updatedAt: new Date(),
        // },
        // {
        //   product_name: "Chana Anda",
        //   product_description: "Chaana Anda",
        //   product_price: 70,
        //   category_id: 5,
        //   createdAt: new Date(),
        //   updatedAt: new Date(),
        // },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("products", null, {});
  },
};
