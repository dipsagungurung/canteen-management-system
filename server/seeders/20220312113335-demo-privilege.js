"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert("privileges", [
      {
        privilege_name: "Create",
        privilege_method: "POST",
      },
      {
        privilege_name: "Read",
        privilege_method: "GET",
      },
      {
        privilege_name: "Update",
        privilege_method: "PUT",
      },
      {
        privilege_name: "Delete",
        privilege_method: "DELETE",
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("privileges", null, {});
  },
};
