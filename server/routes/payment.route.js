const express = require("express");
const paymentController = require("../controllers/payment.controller");
const { makePaymentSchema } = require("../schemas/payment.schema");
const { validator } = require("../middlewares/validator.middleware");
const { authNMiddleware } = require("../middlewares/authN.middleware");
const { authZMiddleware } = require("../middlewares/authZ.middleware");

const paymentRouter = express.Router();

// enable authentication
paymentRouter.use(authNMiddleware);

paymentRouter.get("/payments", paymentController.getAllPayments);

// enable authorization
// paymentRouter.use(authZMiddleware);

paymentRouter.post(
  "/payments",
  validator(makePaymentSchema),
  paymentController.makePayment
);

module.exports = { paymentRouter };
