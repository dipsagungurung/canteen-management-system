const express = require("express");
const { authNMiddleware } = require("../middlewares/authN.middleware");
const profileController = require("../controllers/profile.controller");

const profileRouter = express.Router();

profileRouter.use(authNMiddleware);
profileRouter.get("/profile", profileController.getProfile);

module.exports = { profileRouter };
