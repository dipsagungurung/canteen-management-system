const express = require("express");
const userController = require("../controllers/user.controller");
const { validator } = require("../middlewares/validator.middleware");
const { userIdSchema, userUpdateSchema } = require("../schemas/user.schema");
const { authNMiddleware } = require("../middlewares/authN.middleware");

const userRouter = express.Router();

userRouter.use(authNMiddleware);

userRouter.get("/users", userController.getAllUsers);
userRouter.get(
  "/users/:userId",
  validator(userIdSchema),
  userController.getOneUser
);

userRouter.put(
  "/users/:userId",
  validator(userUpdateSchema),
  userController.updateUser
);

userRouter.delete(
  "/users/:userId",
  validator(userIdSchema),
  userController.deleteUser
);

module.exports = { userRouter };
