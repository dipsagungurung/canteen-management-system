const express = require("express");

const authNController = require("../controllers/authN.controller");
const userController = require("../controllers/user.controller");
const { validator } = require("../middlewares/validator.middleware");
const { userRegisterSchema } = require("../schemas/user.schema");
const { loginSchema } = require("../schemas/authN.schema");

const authNRouter = express.Router();

authNRouter.post(
  "/register",
  validator(userRegisterSchema),
  userController.createUser
);
authNRouter.post("/login", validator(loginSchema), authNController.login);
authNRouter.post("/logout", authNController.logout);

module.exports = { authNRouter };
