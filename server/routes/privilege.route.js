const express = require("express");

const privilegeController = require("../controllers/privilege.controller");
const { authNMiddleware } = require("../middlewares/authN.middleware");
const { authZMiddleware } = require("../middlewares/authZ.middleware");
const { validator } = require("../middlewares/validator.middleware");
const {
  privilegeIdSchema,
  privilegeCreateSchema,
  privilegeUpdateSchema,
  manageModulePrivileges,
} = require("../schemas/privilege.schema");

const privilegeRouter = express.Router();

// enable authentication & authorization
privilegeRouter.use(authNMiddleware);
// privilegeRouter.use(authZMiddleware);

// Add/Remove privileges of a module
privilegeRouter.post(
  "/privileges/manageModulePrivileges",
  validator(manageModulePrivileges),
  privilegeController.manageModulePrivileges
);

// CRUD operations for privilege
privilegeRouter.get("/privileges", privilegeController.getAllPrivileges);
privilegeRouter.post(
  "/privileges",
  validator(privilegeCreateSchema),
  privilegeController.createPrivilege
);
privilegeRouter.put(
  "/privileges/:privilegeId",
  validator(privilegeUpdateSchema),
  privilegeController.updatePrivilege
);
privilegeRouter.delete(
  "/privileges/:privilegeId",
  validator(privilegeIdSchema),
  privilegeController.deletePrivilege
);

module.exports = { privilegeRouter };
