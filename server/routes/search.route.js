const express = require("express");
const searchController = require("../controllers/search.controller");
const { searchSchema } = require("../schemas/search.schema");
const { validator } = require("../middlewares/validator.middleware");

const searchRouter = express.Router();

searchRouter.get("/search", validator(searchSchema), searchController.search);

module.exports = { searchRouter };
