const express = require("express");

const { authNRouter } = require("./authN.route");
const { userRouter } = require("./user.route");
const { roleRouter } = require("./role.route");
const { moduleRouter } = require("./module.route");
const { privilegeRouter } = require("./privilege.route");
const { categoryRouter } = require("./category.route");
const { productRouter } = require("./product.route");
const { orderRouter } = require("./order.route");
const { authNMiddleware } = require("../middlewares/authN.middleware");
const { paymentRouter } = require("./payment.route");
const { profileRouter } = require("./profile.route");

const router = express.Router();

router.get("/checkserver", authNMiddleware, (req, res, next) => {
  res.json({
    message: "Canteen Management System is running!",
  });
});

/* Authentication */
router.use(authNRouter); // => /login, /logout, /register

router.use(userRouter); // => /users
router.use(roleRouter); // => /roles
router.use(moduleRouter); // => /modules
router.use(privilegeRouter); // => /privileges

router.use(profileRouter); // => /profile

router.use(categoryRouter); // => /categories
router.use(productRouter); // => /products

router.use(orderRouter); // => /orders
router.use(paymentRouter); // => /payments

module.exports = { router };
