const express = require("express");

const { validator } = require("../middlewares/validator.middleware");
const {
  roleCreateSchema,
  roleIdSchema,
  roleUpdateSchema,
  manageUserRolesSchema,
  manageRoleAccessSchema,
} = require("../schemas/role.schema");
const roleController = require("../controllers/role.controller");
const { authNMiddleware } = require("../middlewares/authN.middleware");
const { authZMiddleware } = require("../middlewares/authZ.middleware");

const roleRouter = express.Router();

// enable authentication and authorization
roleRouter.use(authNMiddleware);
// roleRouter.use(authZMiddleware);

// Grant/Revoke access of a role
roleRouter.post(
  "/roles/manageRoleAccess",
  validator(manageRoleAccessSchema),
  roleController.manageRoleAccess
);

// add/remove roles for a user
roleRouter.post(
  "/roles/manageUserRoles",
  validator(manageUserRolesSchema),
  roleController.manageUserRoles
);

// CRUD operations for role
roleRouter.get("/roles", roleController.getAllRoles);
roleRouter.post(
  "/roles",
  validator(roleCreateSchema),
  roleController.createRole
);
roleRouter.put(
  "/roles/:roleId",
  validator(roleUpdateSchema),
  roleController.updateRole
);
roleRouter.delete(
  "/roles/:roleId",
  validator(roleIdSchema),
  roleController.deleteRole
);

module.exports = { roleRouter };
