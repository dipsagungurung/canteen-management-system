const express = require("express");
const moduleController = require("../controllers/module.controller");
const { authNMiddleware } = require("../middlewares/authN.middleware");
const { authZMiddleware } = require("../middlewares/authZ.middleware");
const { validator } = require("../middlewares/validator.middleware");
const {
  moduleCreateSchema,
  moduleIdSchema,
  moduleUpdateSchema,
} = require("../schemas/module.schema");

const moduleRouter = express.Router();

// enable authentication & authorization
moduleRouter.use(authNMiddleware);
// moduleRouter.use(authZMiddleware);

moduleRouter.get("/modules", moduleController.getAllModules);
moduleRouter.post(
  "/modules",
  validator(moduleCreateSchema),
  moduleController.createModule
);
moduleRouter.put(
  "/modules/:moduleId",
  validator(moduleUpdateSchema),
  moduleController.updateModule
);
moduleRouter.delete(
  "/modules/:moduleId",
  validator(moduleIdSchema),
  moduleController.deleteModule
);

module.exports = { moduleRouter };
