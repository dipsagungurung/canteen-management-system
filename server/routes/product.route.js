const express = require("express");
const productController = require("../controllers/product.controller");
const { authNMiddleware } = require("../middlewares/authN.middleware");
const { authZMiddleware } = require("../middlewares/authZ.middleware");
const { uploadImage } = require("../middlewares/uploadImage.middleware");
const { validator } = require("../middlewares/validator.middleware");
const {
  productCreateSchema,
  productIdSchema,
  productUpdateSchema,
  proudctGetSchema,
  addImagesToProduct,
  deleteImageFromProduct,
} = require("../schemas/product.schema");

const productRouter = express.Router();

productRouter.get(
  "/products",
  validator(proudctGetSchema),
  productController.getAllProducts
);
productRouter.get(
  "/products/:productId",
  validator(productIdSchema),
  productController.getOneProduct
);

// enable authentication & authorization
productRouter.use(authNMiddleware);
// productRouter.use(authZMiddleware);

productRouter.post(
  "/products/images",
  uploadImage,
  validator(addImagesToProduct),
  productController.addImagesToProduct
);
productRouter.delete(
  "/products/images",
  validator(deleteImageFromProduct),
  productController.deleteImageFromProduct
);

productRouter.post(
  "/products",
  uploadImage,
  validator(productCreateSchema),
  productController.createProduct
);

productRouter.put(
  "/products/:productId",
  validator(productUpdateSchema),
  productController.updateProduct
);

productRouter.delete(
  "/products/:productId",
  validator(productIdSchema),
  productController.deleteProduct
);

module.exports = { productRouter };
