const express = require("express");
const categoryController = require("../controllers/category.controller");
const {
  categoryCreateSchema,
  categoryUpdateSchema,
  categoryIdSchema,
  categoryGetSchema,
} = require("../schemas/category.schema");
const { validator } = require("../middlewares/validator.middleware");
const { authNMiddleware } = require("../middlewares/authN.middleware");
const { authZMiddleware } = require("../middlewares/authZ.middleware");

const categoryRouter = express.Router();

categoryRouter.get(
  "/categories",
  validator(categoryGetSchema),
  categoryController.getAllCategories
);
categoryRouter.get(
  "/categories/:categoryId",
  validator(categoryIdSchema),
  categoryController.getOneCategory
);

// enable authentication & authorization
categoryRouter.use(authNMiddleware);
// categoryRouter.use(authZMiddleware);

categoryRouter.post(
  "/categories",
  validator(categoryCreateSchema),
  categoryController.createCategory
);

categoryRouter.put(
  "/categories/:categoryId",
  validator(categoryUpdateSchema),
  categoryController.updateCategory
);

categoryRouter.delete(
  "/categories/:categoryId",
  validator(categoryIdSchema),
  categoryController.deleteCategory
);

module.exports = { categoryRouter };
