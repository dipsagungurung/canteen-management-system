const express = require("express");

const orderController = require("../controllers/order.controller");
const {
  orderCreateSchema,
  orderIdSchema,
  orderUpdateSchema,
} = require("../schemas/order.schema");
const { validator } = require("../middlewares/validator.middleware");
const { authNMiddleware } = require("../middlewares/authN.middleware");
const { authZMiddleware } = require("../middlewares/authZ.middleware");

const orderRouter = express.Router();

// enable authentication
orderRouter.use(authNMiddleware);

orderRouter.get("/orders", orderController.getAllOrders);
orderRouter.get(
  "/orders/:orderId",
  validator(orderIdSchema),
  orderController.getOneOrder
);
orderRouter.post(
  "/orders",
  validator(orderCreateSchema),
  orderController.createOrder
);

// enable authentication & authorization
// orderRouter.use(authZMiddleware);

orderRouter.put(
  "/orders/:orderId",
  validator(orderUpdateSchema),
  orderController.updateOrder
);

orderRouter.delete(
  "/orders/:orderId",
  validator(orderIdSchema),
  orderController.deleteOrder
);

module.exports = { orderRouter };
