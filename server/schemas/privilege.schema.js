const Joi = require("joi");

const privilegeIdSchema = Joi.object({
  privilegeId: Joi.number().integer().required().greater(0),
});

const privilegeCreateSchema = Joi.object({
  privilege_name: Joi.string().min(3).max(30).required(),
  privilege_method: Joi.string().min(3).max(30).required(),
});

const privilegeUpdateSchema = Joi.object({
  privilegeId: Joi.number().integer().required().greater(0),
  privilege_name: Joi.string().min(3).max(30).required(),
  privilege_method: Joi.string().min(3).max(30).required(),
});

const manageModulePrivileges = Joi.object({
  module_id: Joi.number().integer().required().greater(0),
  privileges: Joi.array().items({
    privilege_id: Joi.number().integer().required().greater(0),
  }),
});

module.exports = {
  privilegeIdSchema,
  privilegeCreateSchema,
  privilegeUpdateSchema,
  manageModulePrivileges,
};
