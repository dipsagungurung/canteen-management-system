const Joi = require("joi");

const proudctGetSchema = Joi.object({
  categoryId: Joi.number().integer().greater(0),
});

const productIdSchema = Joi.object({
  productId: Joi.number().integer().required().greater(0),
});

const productCreateSchema = Joi.object({
  product_name: Joi.string().min(3).max(30).required(),
  product_description: Joi.string(),
  product_price: Joi.number().greater(0),
  category_id: Joi.number().integer().required().greater(0),
  0: Joi.object(), // acccpet 5 images
  1: Joi.object(),
  2: Joi.object(),
  3: Joi.object(),
  4: Joi.object(),
});

const productUpdateSchema = Joi.object({
  productId: Joi.number().integer().required().greater(0), // params
  product_name: Joi.string().min(3).max(30).required(),
  product_description: Joi.string(),
  product_price: Joi.number().greater(0),
});

const addImagesToProduct = Joi.object({
  product_id: Joi.number().integer().required().greater(0), // params
  0: Joi.object(), // acccpet 5 images
  1: Joi.object(),
  2: Joi.object(),
  3: Joi.object(),
  4: Joi.object(),
});

const deleteImageFromProduct = Joi.object({
  productId: Joi.number().integer().required().greater(0), // params
  publicId: Joi.string().required(),
});

module.exports = {
  productIdSchema,
  productCreateSchema,
  productUpdateSchema,
  proudctGetSchema,
  addImagesToProduct,
  deleteImageFromProduct,
};
