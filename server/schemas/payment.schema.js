const Joi = require("joi");

const makePaymentSchema = Joi.object({
  user_id: Joi.number().greater(0).required(),
  payment_amount: Joi.number().greater(0).required(),
  payment_remarks: Joi.string(),
});

module.exports = {
  makePaymentSchema,
};
