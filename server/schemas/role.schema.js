const Joi = require("joi");

const roleIdSchema = Joi.object({
  roleId: Joi.number().integer().required().greater(0),
});

const roleCreateSchema = Joi.object({
  role_name: Joi.string().min(3).max(30).required(),
});

const roleUpdateSchema = Joi.object({
  roleId: Joi.number().integer().required().greater(0),
  role_name: Joi.string().min(3).max(30).required(),
});

const manageUserRolesSchema = Joi.object({
  user_id: Joi.number().integer().required().greater(0),
  roles: Joi.array().items({
    role_id: Joi.number().integer().required().greater(0),
  }),
});

const manageRoleAccessSchema = Joi.object({
  role_id: Joi.number().integer().required().greater(0),
  module_privilege: Joi.array().items({
    module_privilege_id: Joi.number().integer().required().greater(0),
  }),
});

module.exports = {
  roleCreateSchema,
  roleUpdateSchema,
  roleIdSchema,
  manageUserRolesSchema,
  manageRoleAccessSchema,
};
