const Joi = require("joi");

const moduleIdSchema = Joi.object({
  moduleId: Joi.number().integer().required().greater(0),
});

const moduleCreateSchema = Joi.object({
  module_name: Joi.string().min(3).max(30).required(),
  module_endpoint: Joi.string().min(3).max(30).required(),
});

const moduleUpdateSchema = Joi.object({
  moduleId: Joi.number().integer().required().greater(0),
  module_name: Joi.string().min(3).max(30).required(),
  module_endpoint: Joi.string().min(3).max(30).required(),
});

module.exports = {
  moduleCreateSchema,
  moduleIdSchema,
  moduleUpdateSchema,
};
