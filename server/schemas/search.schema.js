const Joi = require("joi");

const searchSchema = Joi.object({
  q: Joi.string().min(1).required(),
});

module.exports = { searchSchema };
