const Joi = require("joi");

const categoryGetSchema = Joi.object({
  format: Joi.string().min(3).max(30),
});

const categoryIdSchema = Joi.object({
  categoryId: Joi.number().integer().required().greater(0),
});

const categoryCreateSchema = Joi.object({
  category_name: Joi.string().min(3).max(30).required(),
  category_description: Joi.string(),
  parent_id: Joi.number().integer().greater(0),
});

const categoryUpdateSchema = Joi.object({
  categoryId: Joi.number().integer().required().greater(0), // params
  category_name: Joi.string().min(3).max(30).required(),
  category_description: Joi.string(),
});

module.exports = {
  categoryCreateSchema,
  categoryUpdateSchema,
  categoryIdSchema,
  categoryGetSchema,
};
