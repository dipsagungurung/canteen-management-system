const Joi = require("joi");

const orderIdSchema = Joi.object({
  orderId: Joi.number().integer().required().greater(0),
});

const orderCreateSchema = Joi.object({
  items: Joi.array().items({
    product_id: Joi.number().integer().required().greater(0),
    product_quantity: Joi.number().integer().required().greater(0),
  }),
});

const orderUpdateSchema = Joi.object({
  orderId: Joi.number().integer().required().greater(0), // params
  order_status: Joi.string().valid("accepted", "rejected", "delivered"),
});

module.exports = {
  orderIdSchema,
  orderCreateSchema,
  orderUpdateSchema,
};
