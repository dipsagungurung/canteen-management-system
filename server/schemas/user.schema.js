const Joi = require("joi");

const userIdSchema = Joi.object({
  userId: Joi.number().integer().required().greater(0),
});

const userRegisterSchema = Joi.object({
  username: Joi.string().required(),
  email: Joi.string().email().required(),
  password: Joi.string()
    // .pattern(new RegExp("^[a-zA-Z0-9]{3,30}$"))
    // .min(8)
    .required(),
  repeat_password: Joi.ref("password"),
});

const userUpdateSchema = Joi.object({});

module.exports = { userRegisterSchema, userIdSchema, userUpdateSchema };
