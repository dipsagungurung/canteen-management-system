const fs = require("fs");
const path = require("path");

const successResponse = (res, status, message, data) => {
  const response = {};
  response.success = true;
  response.status = status || 200;
  response.message = message;
  response.data = data || null;

  res.status(status).json({
    success: response.success,
    status: response.status,
    message: response.message,
    data: response.data,
  });
};

const removeAllFilesFromDirectory = (directoryPath) => {
  fs.readdir(directoryPath, (err, files) => {
    if (err) throw err;

    for (const file of files) {
      fs.unlink(path.join(directoryPath, file), (err) => {
        if (err) throw err;
      });
    }
  });
};

const printAllRegisteredRoutes = (app) => {
  console.log(`
  *********************
  All registered routes
  *********************
  `);
  function print(path, layer) {
    if (layer.route) {
      layer.route.stack.forEach(
        print.bind(null, path.concat(split(layer.route.path)))
      );
    } else if (layer.name === "router" && layer.handle.stack) {
      layer.handle.stack.forEach(
        print.bind(null, path.concat(split(layer.regexp)))
      );
    } else if (layer.method) {
      console.log(
        "%s /%s",
        layer.method.toUpperCase(),
        path.concat(split(layer.regexp)).filter(Boolean).join("/")
      );
    }
  }

  function split(thing) {
    if (typeof thing === "string") {
      return thing.split("/");
    } else if (thing.fast_slash) {
      return "";
    } else {
      var match = thing
        .toString()
        .replace("\\/?", "")
        .replace("(?=\\/|$)", "$")
        .match(/^\/\^((?:\\[.*+?^${}()|[\]\\\/]|[^.*+?^${}()|[\]\\\/])*)\$\//);
      return match
        ? match[1].replace(/\\(.)/g, "$1").split("/")
        : "<complex:" + thing.toString() + ">";
    }
  }

  app._router.stack.forEach(print.bind(null, []));
};

module.exports = {
  successResponse,
  removeAllFilesFromDirectory,
  printAllRegisteredRoutes,
};
