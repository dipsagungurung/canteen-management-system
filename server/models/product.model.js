const { DataTypes } = require("sequelize");
const { Category } = require("./category.model");
const { sequelize } = require("../configs/database.config");

const Product = sequelize.define(
  "products",
  {
    product_id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER,
    },
    product_name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: {
        args: true,
        msg: "Product name already in use",
      },
    },
    product_description: {
      type: DataTypes.TEXT,
    },
    product_price: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    category_id: {
      type: DataTypes.INTEGER,
      references: {
        model: Category,
        key: "category_id",
      },
    },
  },
  {
    // options
  }
);

module.exports = { Product };
