const { DataTypes } = require("sequelize");
const { sequelize } = require("../configs/database.config");
const { ModulePrivilege } = require("./modulePrivilege.model");
const { Role } = require("./role.model");

const Access = sequelize.define(
  "access",
  {
    role_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: Role,
        key: "role_id",
      },
    },
    module_privilege_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: ModulePrivilege,
        key: "module_privilege_id",
      },
    },
  },
  {
    timestamps: false,
    freezeTableName: true,
  }
);

module.exports = { Access };
