const { DataTypes } = require("sequelize");
const { sequelize } = require("../configs/database.config");

const Privilege = sequelize.define(
  "privileges",
  {
    privilege_id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER,
    },
    privilege_name: {
      type: DataTypes.STRING, // Create, Read, Update, Delete
      allowNull: false,
      unique: {
        args: true,
        msg: "Name already in use",
      },
    },
    privilege_method: {
      type: DataTypes.STRING, // GET, POST, PUT, DELETE
      allowNull: false,
      unique: {
        args: true,
        msg: "Method already in use",
      },
    },
  },
  { timestamps: false }
);

module.exports = { Privilege };
