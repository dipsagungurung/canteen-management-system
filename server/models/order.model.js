const { DataTypes } = require("sequelize");

const { sequelize } = require("../configs/database.config");
const { User } = require("./user.model");

const Order = sequelize.define(
  "orders",
  {
    order_id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER,
    },
    order_status: {
      type: DataTypes.ENUM,
      values: ["pending", "accepted", "rejected", "delivered"],
      defaultValue: "pending",
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: User,
        key: "id",
      },
    },
    processed_by: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: User,
        key: "id",
      },
    },
  },
  {}
);

module.exports = { Order };
