const { DataTypes } = require("sequelize");
const { User } = require("./user.model");
const { sequelize } = require("../configs/database.config");

const Payment = sequelize.define(
  "payments",
  {
    payment_id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER,
    },
    payment_amount: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    payment_remarks: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: User,
        key: "id",
      },
    },
    processed_by: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: User,
        key: "id",
      },
    },
  },
  {}
);

module.exports = { Payment };
