const { DataTypes } = require("sequelize");
const { sequelize } = require("../configs/database.config");

const Module = sequelize.define(
  "modules",
  {
    module_id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER,
    },
    module_name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: {
        args: true,
        msg: "Module already exists",
      },
    },
    module_endpoint: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: {
        args: true,
        msg: "Endpoint already taken",
      },
    },
  },
  { timestamps: false }
);

module.exports = { Module };
