const { DataTypes } = require("sequelize");
const { sequelize } = require("../configs/database.config");
const { Module } = require("./module.model");
const { Privilege } = require("./privilege.model");

const ModulePrivilege = sequelize.define(
  "module_privilege",
  {
    module_privilege_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    module_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: Module,
        key: "module_id",
      },
    },
    privilege_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: Privilege,
        key: "privilege_id",
      },
    },
  },
  { timestamps: false, freezeTableName: true }
);

module.exports = { ModulePrivilege };
