const { User } = require("./user.model");
const { Role } = require("./role.model");
const { Module } = require("./module.model");
const { Privilege } = require("./privilege.model");
const { Category } = require("./category.model");
const { Product } = require("./product.model");
const { Order } = require("./order.model");
const { Payment } = require("./payment.model");
const { Account } = require("./account.model");
const { UserRole } = require("./userRole.model");
const { ModulePrivilege } = require("./modulePrivilege.model");
const { Access } = require("./access.model");
const { OrderItem } = require("./orderItem.model");
const { ProductImage } = require("./productImage.model");

/* Authorization */
// many to many relation between `users` and `roles`
// with `user_role` as mapping table
Role.belongsToMany(User, { through: UserRole, foreignKey: "role_id" });
User.belongsToMany(Role, { through: UserRole, foreignKey: "user_id" });
UserRole.belongsTo(User, { foreignKey: "user_id" });
UserRole.belongsTo(Role, { foreignKey: "role_id" });
User.hasMany(UserRole, { foreignKey: "user_id" });
Role.hasMany(UserRole, { foreignKey: "role_id" });

// many to many relation between `modules` and `privileges`
// with `module_privilege` as mapping table
Module.belongsToMany(Privilege, {
  through: ModulePrivilege,
  foreignKey: "module_id",
});
Privilege.belongsToMany(Module, {
  through: ModulePrivilege,
  foreignKey: "privilege_id",
});
ModulePrivilege.belongsTo(Module, { foreignKey: "module_id" });
ModulePrivilege.belongsTo(Privilege, { foreignKey: "privilege_id" });
Module.hasMany(ModulePrivilege, { foreignKey: "module_id" });
Privilege.hasMany(ModulePrivilege, { foreignKey: "privilege_id" });

// Many to manu relation between `module_privilege` and `roles` table
// with `access` as mapping table
Role.belongsToMany(ModulePrivilege, {
  through: Access,
  foreignKey: "role_id",
  as: "role_permissions",
});
ModulePrivilege.belongsToMany(Role, {
  through: Access,
  foreignKey: "module_privilege_id",
});
Access.belongsTo(Role, { foreignKey: "role_id" });
Access.belongsTo(ModulePrivilege, { foreignKey: "module_privilege_id" });
Role.hasMany(Access, { foreignKey: "role_id" });
ModulePrivilege.hasMany(Access, { foreignKey: "module_privilege_id" });

// Self referencing relation within `category`model
Category.hasMany(Category, {
  foreignKey: "parent_id",
  allowNull: true,
  onDelete: "CASCADE",
  as: "children",
});

// one to many relation between `category` and `product`
Category.hasMany(Product, {
  foreignKey: "category_id",
  onDelete: "RESTRICT",
  as: "products",
});
Product.belongsTo(Category, {
  foreignKey: "category_id",
});

// one to many relation between `user` and `order`
User.hasMany(Order, {
  foreignKey: "user_id",
  onDelete: "SET NULL",
  as: "orders",
});
Order.belongsTo(User, {
  foreignKey: "user_id",
});

// many to many relation between  `order` and `product`
// with `order_item` as mapping table
Order.belongsToMany(Product, { through: OrderItem, foreignKey: "order_id" });
Product.belongsToMany(Order, { through: OrderItem, foreignKey: "product_id" });
OrderItem.belongsTo(Product, { foreignKey: "product_id" });
OrderItem.belongsTo(Order, { foreignKey: "order_id" });
Product.hasMany(OrderItem, { foreignKey: "product_id" });
Order.hasMany(OrderItem, { foreignKey: "order_id", as: "items" });

// one to many relation between  `user` and `payment`
User.hasMany(Payment, {
  foreignKey: "user_id",
  onDelete: "RESTRICT",
  as: "payments",
});
Payment.belongsTo(User, {
  foreignKey: "user_id",
  as: "paid_by",
});

// one to one relation between `user` and `account`
User.hasOne(Account, { foreignKey: "user_id" });
Account.belongsTo(User, { foreignKey: "user_id" });

// one to many relation between  `product` and `product_image`
Product.hasMany(ProductImage, {
  foreignKey: "product_id",
  as: "product_images",
});
ProductImage.belongsTo(Product, {
  foreignKey: "product_id",
});

module.exports = {
  User,
  UserRole,
  Role,
  Module,
  Privilege,
  ModulePrivilege,
  Access,
  Category,
  Product,
  ProductImage,
  Order,
  OrderItem,
  Payment,
  Account,
};
