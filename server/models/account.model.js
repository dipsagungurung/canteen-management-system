const { DataTypes } = require("sequelize");
const { sequelize } = require("../configs/database.config");
const { User } = require("./user.model");

const Account = sequelize.define(
  "accounts",
  {
    account_id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER,
    },
    credit_balance: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: User,
        key: "id",
      },
    },
  },
  {
    // Other model options go here
  }
);

module.exports = { Account };
