const { DataTypes } = require("sequelize");
const { sequelize } = require("../configs/database.config");

const Role = sequelize.define(
  "roles",
  {
    role_id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER,
    },
    role_name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: {
        args: true,
        msg: "Role already exists",
      },
    },
  },
  { timestamps: false }
);

module.exports = { Role };
