const { DataTypes } = require("sequelize");
const { sequelize } = require("../configs/database.config");

const Category = sequelize.define(
  "categories",
  {
    category_id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER,
    },
    category_name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: {
        args: true,
        msg: "Category name already in use",
      },
    },
    category_description: {
      type: DataTypes.TEXT,
    },
    parent_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: this.Category,
        key: "category_id",
      },
    },
  },
  {
    // Other model options go here
  }
);

module.exports = { Category };
