const { DataTypes } = require("sequelize");
const { sequelize } = require("../configs/database.config");
const { Role } = require("./role.model");
const { User } = require("./user.model");

const UserRole = sequelize.define(
  "user_role",
  {
    role_id: {
      type: DataTypes.INTEGER,
      references: {
        model: Role,
        key: "role_id",
      },
    },
    user_id: {
      type: DataTypes.INTEGER,
      references: {
        model: User,
        key: "id",
      },
    },
  },
  { timestamps: false, freezeTableName: true }
);

module.exports = { UserRole };
