const { DataTypes } = require("sequelize");
const { Product } = require("./product.model");
const { sequelize } = require("../configs/database.config");

const ProductImage = sequelize.define(
  "product_image",
  {
    product_image_id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER,
    },
    public_id: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    product_image_url: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    product_id: {
      type: DataTypes.INTEGER,
      references: {
        model: Product,
        key: "product_id",
      },
    },
  },
  {
    timestamps: false,
    freezeTableName: true,
  }
);

module.exports = { ProductImage };
