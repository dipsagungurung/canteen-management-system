const { Op } = require("sequelize");

const AuthorizationException = require("../exceptions/authorizationException");
const { Access, ModulePrivilege, Module, Privilege } = require("../models");

const authZMiddleware = async (req, res, next) => {
  // check if  `user` is set by authentication middleware
  if (req.user === undefined || req.user === null) {
    next(new AuthorizationException());
  }

  // check if `user` has `role`
  if (req.user.roles.length === 0) {
    next(new AuthorizationException());
  }

  const user = req.user;
  const method = req.method;
  const url = req.url;

  // find `module` and `privilege`
  const module = await Module.findOne({
    where: {
      module_endpoint: url,
    },
  });
  const privilege = await Privilege.findOne({
    where: {
      privilege_method: method,
    },
  });

  // find mapping between `module` and `privilege`
  const modulePrivilege = await ModulePrivilege.findOne({
    where: {
      module_id: module.module_id,
      privilege_id: privilege.privilege_id,
    },
  });

  // create filter object to query the `access` table
  const filter = user.roles.map((role) => {
    return {
      role_id: role.role_id,
      module_privilege_id: modulePrivilege.module_privilege_id,
    };
  });
  const access = await Access.findAll({
    where: {
      [Op.or]: filter,
    },
  });

  // chech if the user has `access` to use `method` on `module`
  if (access.length === 0) {
    next(new AuthorizationException());
  }

  // `user` is authorzied pass the control to next middleware
  next();
};

module.exports = { authZMiddleware };
