const { passport } = require("../configs/passport.config");
const AuthorizationException = require("../exceptions/authorizationException");

const authNMiddleware = (req, res, next) => {
  passport.authenticate("jwt", { session: false }, function (err, user, info) {
    // If authentication fails user will be set to false
    // err will be set and info will be set with the stack trace
    if (err || !user) {
      next(new AuthorizationException()); // to error handler
    } else {
      // authentication succeeds
      // attach the current user to the req object as req.user
      req.user = user;
      next();
    }
  })(req, res, next);
};

module.exports = { authNMiddleware };
