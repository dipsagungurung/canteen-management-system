const path = require("path");

const multer = require("multer");
const { nanoid } = require("nanoid");

const { ValidationException } = require("../exceptions/validationException");

const uploadImage = (req, res, next) => {
  // for storing images at './public/uploads'
  const fileStorageEngine = multer.diskStorage({
    destination: (req, file, callback) => {
      callback(null, "./public/uploads");
    },
    filename: (req, file, callback) => {
      let fileExtension = path.extname(file.originalname);
      fileExtension = fileExtension.toLowerCase();
      let name = `HCImg_${nanoid(12)}${fileExtension}`;
      callback(null, name);
    },
  });

  // Max number of files user can upload at a time. Default
  const FILE_MAX_COUNT = 5;

  const fileFilter = (req, file, callback) => {
    const mimetype = file.mimetype;
    const mimetypeSplitted = mimetype.split("/");
    const ext = mimetypeSplitted[1];
    if (
      ext !== "png" &&
      ext !== "jpeg" &&
      ext !== "jpg" &&
      ext !== "JPEG" &&
      ext !== "JPG"
    ) {
      callback(new ValidationException("Only images are allowed"));
    }
    callback(null, true);
  };

  // Multer setup
  const upload = multer({
    storage: multer.memoryStorage(),
    fileFilter: fileFilter,
  }).array("images", FILE_MAX_COUNT);

  try {
    // To handle multer errors with express
    upload(req, res, function (err) {
      if (err instanceof multer.MulterError) {
        next(new ValidationException(`Image validation error: ${err.message}`));
      } else if (err) {
        next(err);
      }
      next();
    });
  } catch (err) {
    next(err);
  }
};

module.exports = { uploadImage };
