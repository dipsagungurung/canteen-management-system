const { ValidationException } = require("../exceptions/validationException");

const validator = (schema) => {
  return (req, res, next) => {
    try {
      // validate against request body and request params
      const { error, value } = schema.validate(
        {
          ...req.body,
          ...req.params,
          ...req.query,
          ...req.file,
          ...req.files,
        },
        { abortEarly: false }
      );
      if (error) {
        let errorMessages = [];
        errorMessages = error.details.map((curr) => {
          return curr.message;
        });

        throw new ValidationException(errorMessages);
      }
      next(); // proceed if validation doesn't fail
    } catch (err) {
      next(err);
    }
  };
};

module.exports = { validator };
