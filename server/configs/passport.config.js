const passport = require("passport");
const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;
// const LocalStrategy = require("passport-local").Strategy;

// To use JWT strategy
/**
 * Every possible options
 * 
  const passportJwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken,
  secretOrKey: process.env.JWT_SECRET_KEY,
  issuer: "enter issuer here",
  audiece: "enter audience here",
  algorithms: ["RS256"],
  ignoreExpiration: false,
  passReqToCallback: flase,
  jsonWebTokenOptions: {
    complete: false,
    clockTolerance: "",
    maxAge: "2d", //2 days
    clockTimestamp: "100",
    nonce: "string here for openID",
  },
 };
 * 
*/

/* Validating JWT token */
const passportJwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(), // Bearer <token>
  secretOrKey: process.env.JWT_SECRET_KEY,
};

/**
 * payload: {
    id: user.id,
    email: user.email,
    username: user.username,
    roles: user.roles,
    iat: Date.now(),
 * }
 */
const passportJwtCallback = async (payload, done) => {
  try {
    done(null, payload);
  } catch (err) {
    done(err, false);
  }
};

passport.use(new JwtStrategy(passportJwtOptions, passportJwtCallback));

module.exports = { passport };

/**
 * 
 // To use local strategy
passport.use(
  new LocalStrategy(
    // options
    {
      usernameField: "email",
    },
    // verify function
    async (email, password, done) => {
      try {
        const user = await UserService.findOneUserByEmail(email);

        if (!user) throw new AuthenticationException();

        if (!(await UserService.verifyUserPassword(user, password)))
          throw new AuthenticationException();

        return done(null, user);
      } catch (err) {
        return done(err);
      }
    }
  )
);

passport.serializeUser(function (user, cb) {
  process.nextTick(function () {
    cb(null, { id: user.id, email: user.email });
  });
});

passport.deserializeUser(function (user, cb) {
  process.nextTick(function () {
    return cb(null, user);
  });
});
 * 
*/
