const { MeiliSearch } = require("meilisearch");

// Loading the .env file
require("dotenv").config();

const meilisearchClient = new MeiliSearch({
  host: process.env.MEILISEARCH_HOST,
});

module.exports = { meilisearchClient };
