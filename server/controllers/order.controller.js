const orderService = require("../services/order.service");
const { successResponse } = require("../utils/helper.util");

class OrderController {
  async getAllOrders(req, res, next) {
    try {
      const orders = await orderService.getAllOrders(req.user);

      successResponse(res, 200, "All orders", orders);
    } catch (err) {
      next(err);
    }
  }

  async createOrder(req, res, next) {
    try {
      await orderService.createOrder(req.user, req.body);

      successResponse(res, 201, "Order created", null);
    } catch (err) {
      next(err);
    }
  }

  async getOneOrder(req, res, next) {
    try {
      const order = await orderService.getOneOrder(req.user, req.params);

      successResponse(res, 200, "Order fethced", order);
    } catch (err) {
      next(err);
    }
  }

  async updateOrder(req, res, next) {
    try {
      await orderService.updateOrder(req.user, req.params, req.body);
      successResponse(res, 200, "Order updated", null);
    } catch (err) {
      next(err);
    }
  }

  async deleteOrder(req, res, next) {
    try {
      await orderService.deleteOrder(req.user, req.params);
      successResponse(res, 200, "Order deleted", null);
    } catch (err) {
      next(err);
    }
  }
}

module.exports = new OrderController();
