const categoryService = require("../services/category.service");
const { successResponse } = require("../utils/helper.util");

class CategoryController {
  async getAllCategories(req, res, next) {
    try {
      const categories = await categoryService.getAllCategories(req.query);

      successResponse(res, 200, "Categories fetched", categories);
    } catch (err) {
      next(err);
    }
  }

  async getOneCategory(req, res, next) {
    try {
      const { categoryId } = req.params;
      const category = await categoryService.getOneCategory(categoryId);

      successResponse(res, 200, "Category fetched", category);
    } catch (err) {
      next(err);
    }
  }

  async createCategory(req, res, next) {
    try {
      await categoryService.createCategory(req.body);

      successResponse(res, 201, "Category created", null);
    } catch (err) {
      next(err);
    }
  }

  async updateCategory(req, res, next) {
    try {
      await categoryService.updateCategory(req.params, req.body);

      successResponse(res, 201, "Category updated", null);
    } catch (err) {
      next(err);
    }
  }

  async deleteCategory(req, res, next) {
    try {
      await categoryService.deleteCategory(req.params);

      successResponse(res, 200, "Category deleted", null);
    } catch (err) {
      next(err);
    }
  }
}

module.exports = new CategoryController();
