const paymentService = require("../services/payment.service");
const { successResponse } = require("../utils/helper.util");

class PaymentController {
  async getAllPayments(req, res, next) {
    try {
      const payments = await paymentService.getAllPayments(req.user);

      successResponse(res, 200, "All payments", payments);
    } catch (err) {
      next(err);
    }
  }

  // only authorized person: `staff` or `manager` or `admin` can make payment
  // `customer` cannot directly make payment
  async makePayment(req, res, next) {
    try {
      await paymentService.makePayment(req.user, req.body);

      successResponse(res, 200, "Payment made successfully", null);
    } catch (err) {
      next(err);
    }
  }
}

module.exports = new PaymentController();
