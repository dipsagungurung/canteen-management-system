const { successResponse } = require("../utils/helper.util");
const searchService = require("../services/search.service");

class SearchController {
  async search(req, res, next) {
    try {
      const result = await searchService.search(req.query);

      successResponse(res, 200, "Search result", result);
    } catch (err) {
      next(err);
    }
  }
}

module.exports = new SearchController();
