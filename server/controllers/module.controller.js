const { successResponse } = require("../utils/helper.util");
const moduleService = require("../services/module.service");

class ModuleController {
  async createModule(req, res, next) {
    try {
      await moduleService.createModule(req.body);

      successResponse(res, 201, "Module created", null);
    } catch (err) {
      next(err);
    }
  }

  async getAllModules(req, res, next) {
    try {
      const modules = await moduleService.getAllModules();

      successResponse(res, 200, "All modules", modules);
    } catch (err) {
      next(err);
    }
  }

  async updateModule(req, res, next) {
    try {
      const { moduleId } = req.params;
      await moduleService.updateModule(moduleId, req.body);

      successResponse(res, 201, "Module updated", null);
    } catch (err) {
      next(err);
    }
  }

  async deleteModule(req, res, next) {
    try {
      const { moduleId } = req.params;
      await moduleService.deleteModule(moduleId);

      successResponse(res, 200, "Module deleted", null);
    } catch (err) {
      next(err);
    }
  }
}

module.exports = new ModuleController();
