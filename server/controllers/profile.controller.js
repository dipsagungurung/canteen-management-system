const profileService = require("../services/profile.service");
const { successResponse } = require("../utils/helper.util");

class ProfileController {
  async getProfile(req, res, next) {
    try {
      const profile = await profileService.getProfile(req.user);

      successResponse(res, 200, "Profile fetched successfully", profile);
    } catch (err) {
      next(err);
    }
  }
}

module.exports = new ProfileController();
