const { successResponse } = require("../utils/helper.util");
const roleService = require("../services/role.service");

class RoleController {
  async getAllRoles(req, res, next) {
    try {
      const roles = await roleService.getAllRoles();

      successResponse(res, 200, "All Roles", roles);
    } catch (err) {
      next(err);
    }
  }

  async createRole(req, res, next) {
    try {
      await roleService.createRole(req.body);

      successResponse(res, 201, "Role created", null);
    } catch (err) {
      next(err);
    }
  }

  async updateRole(req, res, next) {
    try {
      const { roleId } = req.params;
      await roleService.updateRole(roleId, req.body);

      successResponse(res, 201, "Role updated", null);
    } catch (err) {
      next(err);
    }
  }

  async deleteRole(req, res, next) {
    try {
      const { roleId } = req.params;
      await roleService.deleteRole(roleId);

      successResponse(res, 200, "Role deleted", null);
    } catch (err) {
      next(err);
    }
  }

  async manageUserRoles(req, res, next) {
    try {
      await roleService.manageUserRoles(req.body);

      successResponse(res, 200, "User roles updated successfully", null);
    } catch (err) {
      next(err);
    }
  }

  async addAccessToRole(req, res, next) {
    try {
      await roleService.addAccessToRole(req.body);

      successResponse(res, 200, "Access has been granted to the role", null);
    } catch (err) {
      next(err);
    }
  }

  async removeAccessFromRole(req, res, next) {
    try {
      await roleService.removeAccessFromRole(req.body);

      successResponse(res, 200, "Access has been removed from the role", null);
    } catch (err) {
      next(err);
    }
  }

  async manageRoleAccess(req, res, next) {
    try {
      await roleService.manageRoleAccess(req.body);

      successResponse(res, 200, "Role Access updated successfully", null);
    } catch (err) {
      next(err);
    }
  }
}

module.exports = new RoleController();
