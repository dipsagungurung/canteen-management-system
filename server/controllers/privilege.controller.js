const privilegeService = require("../services/privilege.service");
const { successResponse } = require("../utils/helper.util");

class PrivilegeController {
  async createPrivilege(req, res, next) {
    try {
      await privilegeService.createPrivilege(req.body);

      successResponse(res, 201, "Privilege created", null);
    } catch (err) {
      next(err);
    }
  }

  async getAllPrivileges(req, res, next) {
    try {
      const privileges = await privilegeService.getAllPrivileges();

      successResponse(res, 201, "All privileges", privileges);
    } catch (err) {
      next(err);
    }
  }

  async updatePrivilege(req, res, next) {
    try {
      const { privilegeId } = req.params;
      await privilegeService.updatePrivilege(privilegeId, req.body);

      successResponse(res, 201, "Privilege updated", null);
    } catch (err) {
      next(err);
    }
  }

  async deletePrivilege(req, res, next) {
    try {
      const { privilegeId } = req.params;
      await privilegeService.deletePrivilege(privilegeId);

      successResponse(res, 200, "Privilege deleted", null);
    } catch (err) {
      next(err);
    }
  }

  async manageModulePrivileges(req, res, next) {
    try {
      await privilegeService.manageModulePrivileges(req.body);

      successResponse(res, 200, "Module Privileges mapped successfully", null);
    } catch (err) {
      next(err);
    }
  }
}

module.exports = new PrivilegeController();
