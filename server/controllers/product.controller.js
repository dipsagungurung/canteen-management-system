const productService = require("../services/product.service");
const { successResponse } = require("../utils/helper.util");

class ProductController {
  async createProduct(req, res, next) {
    try {
      await productService.createProduct(req.body, req.files);

      successResponse(res, 201, "Product created", null);
    } catch (err) {
      next(err);
    }
  }

  async getAllProducts(req, res, next) {
    try {
      const products = await productService.getAllProducts(req.query);

      successResponse(res, 200, "All products", products);
    } catch (err) {
      next(err);
    }
  }

  async getOneProduct(req, res, next) {
    try {
      const { productId } = req.params;
      const product = await productService.getOneProduct(productId);

      successResponse(res, 200, "Product fetched", product);
    } catch (err) {
      next(err);
    }
  }

  async updateProduct(req, res, next) {
    try {
      await productService.updateProduct(req.params, req.body);

      successResponse(res, 201, "Product updated", null);
    } catch (err) {
      next(err);
    }
  }

  async deleteProduct(req, res, next) {
    try {
      await productService.deleteProduct(req.params);

      successResponse(res, 201, "Product deleted", null);
    } catch (err) {
      next(err);
    }
  }

  async addImagesToProduct(req, res, next) {
    try {
      await productService.addImagesToProduct(req.body, req.files);

      successResponse(
        res,
        200,
        "Images added to the product successfully",
        null
      );
    } catch (err) {
      next(err);
    }
  }

  async deleteImageFromProduct(req, res, next) {
    try {
      await productService.deleteImageFromProduct(req.query);

      successResponse(
        res,
        201,
        "Image deleted from the product successfully",
        null
      );
    } catch (err) {
      next(err);
    }
  }
}

module.exports = new ProductController();
