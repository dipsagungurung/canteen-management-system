const authNService = require("../services/authN.service");
const { successResponse } = require("../utils/helper.util");

class AuthNController {
  async login(req, res, next) {
    try {
      const { email, password } = req.body;

      const validUser = await authNService.verifyLoginCredentials(
        email.toLowerCase(),
        password
      );
      const token = await authNService.issueJwtToken(validUser);

      successResponse(res, 200, "Logged in successfully", {
        token: token,
      });
    } catch (err) {
      next(err);
    }
  }

  logout(req, res, next) {
    try {
      // since session is managed using jwt for now just send logged out
      // track the jwt-tokens? and invalidate it?
      // use other methods to keep session

      successResponse(res, "200", "Logged out", null);
    } catch (err) {
      next(err);
    }
  }
}

module.exports = new AuthNController();
