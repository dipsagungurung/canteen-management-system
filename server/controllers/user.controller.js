const userService = require("../services/user.service");
const { successResponse } = require("../utils/helper.util");

class UserController {
  async createUser(req, res, next) {
    try {
      await userService.createUser(req.body);

      successResponse(res, 200, "User created", null);
    } catch (err) {
      next(err);
    }
  }

  async getAllUsers(req, res, next) {
    try {
      const users = await userService.getAllUsers(req.user);

      successResponse(res, 200, "All users", users);
    } catch (err) {
      next(err);
    }
  }

  async getOneUser(req, res, next) {
    try {
      const user = await userService.getOneUser(req.user, req.params);

      successResponse(res, 200, "User fetched", user);
    } catch (err) {
      next(err);
    }
  }

  // TODO
  async updateUser() {}

  async deleteUser(req, res, next) {
    try {
      await userService.deleteUser(req.user, req.params);

      successResponse(res, 200, "User deleted", null);
    } catch (err) {
      next(err);
    }
  }
}

module.exports = new UserController();
