class AuthNException extends Error {
  constructor() {
    super("Invalid email or password");
    this.status = 401;
    this.message = "Invalid email or password";
  }
}

module.exports = AuthNException;
