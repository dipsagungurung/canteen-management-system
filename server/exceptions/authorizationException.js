class AuthorizationException extends Error {
  constructor() {
    super("Not authorized");
    this.status = 401;
    this.message = "Not authorized";
  }
}

module.exports = AuthorizationException;
