const categoryRepository = require("../repositories/category.repository");

class CategoryService {
  async getAllCategories(query) {
    try {
      const { format } = query; // `tree` or `list`

      let categories;
      switch (format) {
        case "tree":
          categories = await categoryRepository.getAllCategoriesAsTree();
          break;
        case "list":
          categories = await categoryRepository.getAllCategoriesAsList();
          break;
        default:
          categories = await categoryRepository.getAllCategoriesAsList();
          break;
      }

      return categories;
    } catch (err) {
      throw err;
    }
  }

  async getOneCategory(categoryId) {
    try {
      const category = await categoryRepository.getOneCategory(categoryId);
      return category;
    } catch (err) {
      throw err;
    }
  }

  async createCategory(body) {
    try {
      const { parent_id } = body;

      // if user is trying to make a `sub-category`
      if (parent_id) {
        const parentCategory = await categoryRepository.getOneCategory(
          parent_id
        );

        // check if you are allowed to make sub category in the category
        const plainParentCategory = parentCategory.get({ plain: true });
        if (plainParentCategory.parent_id != null) {
          throw new ValidationException(
            "Cannot make child category on the given category"
          );
        }
      }

      // create the `category`
      await categoryRepository.createCategory(body);
    } catch (err) {
      throw err;
    }
  }

  async updateCategory(params, body) {
    try {
      // find the category
      const { categoryId } = params;
      const category = await categoryRepository.getOneCategory(categoryId);

      // update the category
      await categoryRepository.updateCategory(category, body);
    } catch (err) {
      throw err;
    }
  }

  async deleteCategory(params) {
    try {
      const { categoryId } = params;
      await categoryRepository.deleteCategory(categoryId);
    } catch (err) {
      throw err;
    }
  }
}

module.exports = new CategoryService();
