const { UniqueConstraintError } = require("sequelize");

const { ValidationException } = require("../exceptions/validationException");
const AuthorizationException = require("../exceptions/authorizationException");
const userRepository = require("../repositories/user.repository");

class UserService {
  async createUser(body) {
    try {
      // hash the password
      const { password } = body;
      const hashedPassword = await userRepository.hashUserPassword(password);

      // user data
      const userData = {
        username: body.username,
        email: body.email.toLowerCase(),
        password: hashedPassword,
      };

      // create the user
      const user = await userRepository.createUser(userData);

      // create an account with zero balance for the user
      await userRepository.createUserAccount(user.id);
    } catch (err) {
      if (err instanceof UniqueConstraintError) {
        throw new ValidationException(err.message);
      }
      throw err;
    }
  }

  async getAllUsers(user) {
    try {
      // check if the user is `admin` or `manager`
      if (user.roles.includes("admin") || user.roles.includes("manager")) {
        const users = await userRepository.getAllUsers();
        return users;
      }

      throw new AuthorizationException();
    } catch (err) {
      throw err;
    }
  }

  async getOneUser(user, params) {
    try {
      const { userId } = params;
      const requestedUser = userRepository.getOneUser(userId);

      if (user.roles.includes("admin") || user.roles.include("manager")) {
        return requestedUser;
      }

      // if the user is requesting his/her own profile
      if (user.id === requestedUser.id) {
        return requestedUser;
      }

      throw new AuthorizationException();
    } catch (err) {
      throw err;
    }
  }

  // TODO:
  async updateUser() {}

  async deleteUser(user, params) {
    try {
      const { userId } = params;
      if (user.roles.includes("admin") || user.roles.include("manager")) {
        await userRepository.deleteUser(userId);
      }

      // if the user is trying to delete his/her own profile
      if (user.id === userId) {
        await userRepository.deleteUser(userId);
      }

      throw new AuthorizationException();
    } catch (err) {
      throw err;
    }
  }
}

module.exports = new UserService();
