const { UniqueConstraintError } = require("sequelize");

const { NotFoundException } = require("../exceptions/notFoundException");
const { ValidationException } = require("../exceptions/validationException");
const { Module, Privilege, ModulePrivilege } = require("../models");

class ModuleService {
  async createModule(moduleData) {
    try {
      await Module.create(moduleData);
    } catch (err) {
      if (err instanceof UniqueConstraintError) {
        throw new ValidationException(err.message);
      }
      throw err;
    }
  }

  async getAllModules() {
    const modules = await Module.findAll({
      include: [
        {
          model: Privilege,
          through: {
            model: ModulePrivilege,
            attributes: ["module_privilege_id"],
          },
        },
      ],
    });

    if (modules.length === 0) throw new NotFoundException();
    return modules;
  }

  async getOneModule(moduleId) {
    const module = await Module.findOne({
      where: {
        module_id: moduleId,
      },
    });

    if (module === null || module === undefined)
      throw new NotFoundException("Module not found");

    return module;
  }

  async updateModule(moduleId, moduleData) {
    const module = await this.getOneModule(moduleId);

    await module.update(moduleData);
  }

  async deleteModule(moduleId) {
    const module = await this.getOneModule(moduleId);

    await module.destroy();
  }
}

module.exports = new ModuleService();
