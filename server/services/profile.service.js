const profileRepository = require("../repositories/profile.repository");

class ProfileService {
  async getProfile(user) {
    try {
      const profile = await profileRepository.getProfile(user.id);
      return profile;
    } catch (err) {
      throw err;
    }
  }
}

module.exports = new ProfileService();
