const {
  UniqueConstraintError,
  ForeignKeyConstraintError,
} = require("sequelize");
const { sequelize } = require("../configs/database.config");

const { Privilege } = require("../models");
const { NotFoundException } = require("../exceptions/notFoundException");
const { ValidationException } = require("../exceptions/validationException");
const privilegeRepository = require("../repositories/privilege.repository");

class PrivilegeService {
  async createPrivilege(privilegeData) {
    try {
      await Privilege.create(privilegeData);
    } catch (err) {
      if (err instanceof UniqueConstraintError) {
        throw new ValidationException(err.message);
      }
      throw err;
    }
  }

  async getAllPrivileges() {
    const privileges = await Privilege.findAll();
    if (privileges.length === 0) throw new NotFoundException();
    return privileges;
  }

  async getOnePrivilege(privilegeId) {
    const privilege = await Privilege.findOne({
      where: {
        privilege_id: privilegeId,
      },
    });

    if (privilege === null || privilege === undefined)
      throw new NotFoundException();
    return privilege;
  }

  async updatePrivilege(privilegeId, privilegeData) {
    const privilege = await this.getOnePrivilege(privilegeId);
    await privilege.update(privilegeData);
  }

  async deletePrivilege(privilegeId) {
    const privilege = await this.getOnePrivilege(privilegeId);
    await privilege.destroy();
  }

  async manageModulePrivileges(body) {
    const transaction = await sequelize.transaction();
    try {
      const { module_id } = body;

      // destroy previous mappings of the module
      await privilegeRepository.destroyModuleMappings(transaction, module_id);

      // create new mapping according to the mappingData
      const mappingData = body.privileges.map((currValue) => {
        return {
          privilege_id: currValue.privilege_id,
          module_id: module_id,
        };
      });
      await privilegeRepository.createModuleMappings(transaction, mappingData);

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();

      if (err instanceof ForeignKeyConstraintError) {
        throw new ValidationException("Privilege doesnot exists");
      }
      throw err;
    }
  }
}

module.exports = new PrivilegeService();
