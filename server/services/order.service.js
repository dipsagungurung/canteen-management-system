const { ForeignKeyConstraintError } = require("sequelize");

const { sequelize } = require("../configs/database.config");
const { ValidationException } = require("../exceptions/validationException");
const { HttpException } = require("../exceptions/httpException");
const orderRepository = require("../repositories/order.repository");
const AuthorizationException = require("../exceptions/authorizationException");

class OrderService {
  async getAllOrders(user) {
    // check if the user is `admin` or `manager`
    if (user.roles.includes("admin") || user.roles.includes("manager")) {
      const orders = await orderRepository.getAllOrders();
      return orders;
    }

    // check if the user is `staff`
    if (user.roles.includes("staff")) {
      const orders = await orderRepository.getAllTodaysOrders();
      return orders;
    }

    // check if the user is `customer`
    if (user.roles.length === 0) {
      const orders = await orderRepository.getAllUserOrders(user.id);
      return orders;
    }
  }

  async createOrder(user, orderData) {
    const transaction = await sequelize.transaction();
    try {
      // create an order with the user_id
      const order = await orderRepository.createOrder(transaction, user.id);

      // add ordered items with its `quantity` & `price` to the created `order`
      await orderRepository.addItemsToOrder(transaction, order, orderData);

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      if (err instanceof ForeignKeyConstraintError) {
        throw new ValidationException("Product ordered doesnot exist");
      }
      throw err;
    }
  }

  async getOneOrder(user, params) {
    try {
      const { orderId } = params;

      // administration can get any order
      if (
        user.roles.includes("admin") ||
        user.roles.includes("manager") ||
        user.roles.includes("staff")
      ) {
        const order = await orderRepository.getOneOrder(orderId);
        return order;
      }

      // customer can get only his order
      if (user.roles.length === 0) {
        const order = await orderRepository.getOneUserOrder(user.id, orderId);
        return order;
      }
    } catch (err) {
      throw err;
    }
  }

  async updateOrder(processedByUser, params, body) {
    const transaction = await sequelize.transaction();
    try {
      const { orderId } = params;
      const order = await orderRepository.getOneOrder(orderId);

      // `customer` cannot update the order
      if (processedByUser.roles.length === 0) {
        throw new HttpException(403, "You cannot update the order");
      }

      // if order is already `rejected` or `delivered`
      // and if order was tried to be `accepted` twice
      if (
        order.order_status === "rejected" ||
        order.order_status === "delivered" ||
        (order.order_status === "accepted" && body.order_status === "accepted")
      ) {
        throw new HttpException(
          403,
          `Order is already ${order.order_status} cannot ${body.order_status} the order`
        );
      }

      // if the order was `rejected` notify the user
      // TODO: complete the block
      if (body.order_status === "rejected") {
        await orderRepository.updateOrder(
          transaction,
          processedByUser,
          order.order_id,
          body
        );
        await transaction.commit();
        return;
      }

      // if the order was `accepted` or `delivered` from `pending` state
      // credit the user account & update the order
      if (
        (order.order_status === "pending" &&
          body.order_status === "accepted") ||
        (order.order_status === "pending" && body.order_status === "delivered")
      ) {
        const totalPrice = await orderRepository.computeTotalPriceOfOrder(
          order.order_id
        );
        // add `total_price` of the `order` to users `account`
        await orderRepository.creditUserAccount(
          transaction,
          order.user_id,
          totalPrice
        );
        await orderRepository.updateOrder(
          transaction,
          processedByUser,
          order.order_id,
          body
        );
        await transaction.commit();
        return;
      }

      // if the order was `delivered` from `accepted` state
      if (
        order.order_status === "accepted" &&
        body.order_status === "delivered"
      ) {
        await orderRepository.updateOrder(
          transaction,
          processedByUser,
          order.order_id,
          body
        );
        await transaction.commit();
        return;
      }
    } catch (err) {
      transaction.rollback();
      throw err;
    }
  }

  async deleteOrder(user, params) {
    try {
      const { orderId } = params;
      const order = await orderRepository.getOneOrder(orderId);

      // check if the user is `admin`
      if (user.roles.includes("admin")) {
        await orderRepository.deleteOrder(orderId);
        return;
      }

      // check if order status is still `pending`
      // pending order can be deleted by managers
      if (user.roles.includes("manager") && order.order_status === "pending") {
        await orderRepository.deleteOrder(orderId);
        return;
      }

      // check if the order belongs to the customer
      if (order.user_id !== user.id) {
        throw new AuthorizationException();
      }

      // delete the order only if
      // the order belongs to the user and order status is still `pending`
      if (order.user_id === user.id && order.order_status === "pending") {
        await orderRepository.deleteOrder(orderId);
        return;
      }

      // if none of the above conditions is met then
      // the user is not an admin and the order is already being processed
      throw new HttpException(
        403,
        "Sorry! Cannot delete the order since the order is already proccessed. Only pending orders can be deleted."
      );
    } catch (err) {
      throw err;
    }
  }
}

module.exports = new OrderService();
