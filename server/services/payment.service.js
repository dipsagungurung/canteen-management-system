const paymentRepository = require("../repositories/payment.repository");
const { sequelize } = require("../configs/database.config");

class PaymentService {
  // only authorized person: `staff` or `manager` or `admin` can make payment
  // `customer` cannot directly make payment
  async makePayment(processedByUser, paymentData) {
    const transaction = await sequelize.transaction();
    try {
      await paymentRepository.getOneUser(paymentData.user_id);

      // add the payment info to `payments` table
      await paymentRepository.createPayment(
        transaction,
        processedByUser,
        paymentData
      );

      // deduct the `credit_balance` of the customer from `accounts` table
      await paymentRepository.deductCreditFromUserAccount(
        transaction,
        paymentData
      );

      // create receipt?

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  }

  async getAllPayments(user) {
    try {
      // check if the user is `admin` or `manager`
      if (user.roles.includes("admin") || user.roles.includes("manager")) {
        const payments = await paymentRepository.getAllPayments();
        return payments;
      }

      // check if the user is `staff`
      if (user.roles.includes("staff")) {
        const payments = await paymentRepository.getTodaysPayments();
        return payments;
      }

      // check if the user is `customer`
      if (user.roles.length === 0) {
        const payments = await paymentRepository.getUserPayments(user.id);
        return payments;
      }
    } catch (err) {
      throw err;
    }
  }
}

module.exports = new PaymentService();
