const { UniqueConstraintError } = require("sequelize");

const { sequelize } = require("../configs/database.config");
const {
  Role,
  UserRole,
  ModulePrivilege,
  Access,
  Privilege,
  Module,
} = require("../models");
const { NotFoundException } = require("../exceptions/notFoundException");
const { ValidationException } = require("../exceptions/validationException");
const moduleService = require("./module.service");
const privilegeService = require("./privilege.service");
const userRepository = require("../repositories/user.repository");
const roleRepository = require("../repositories/role.repository");

class RoleService {
  async getAllRoles() {
    const roles = await Role.findAll({
      include: [
        {
          model: ModulePrivilege,
          as: "role_permissions",
          attributes: ["module_privilege_id"],
          through: { attributes: [] },
          include: [
            {
              model: Privilege,
              attributes: ["privilege_id", "privilege_method"],
            },
            {
              model: Module,
              attributes: ["module_id", "module_name", "module_endpoint"],
            },
          ],
        },
      ],
    });

    if (roles.length === 0) throw new NotFoundException();
    return roles;
  }

  async getOneRole(roleId) {
    const role = await Role.findOne({
      where: {
        role_id: roleId,
      },
    });

    if (role === null || role === undefined)
      throw new NotFoundException("Role not found");
    return role;
  }

  async createRole(roleData) {
    try {
      roleData = { ...roleData, role_name: roleData.role_name.toLowerCase() };
      await Role.create(roleData);
    } catch (err) {
      if (err instanceof UniqueConstraintError) {
        throw new ValidationException(err.message);
      }
      throw err;
    }
  }

  async updateRole(roleId, roleData) {
    const role = await this.getOneRole(roleId);
    await role.update(roleData);
  }

  async deleteRole(roleId) {
    const role = await this.getOneRole(roleId);
    await role.destroy();
  }

  async addUserToRole(mappingData) {
    const role = await this.getOneRole(mappingData.role_id);
    const user = await userRepository.getOneUser(mappingData.user_id);

    // check if mapping already exists
    const isAlreadyAssigned = await UserRole.findOne({
      where: mappingData,
    });

    if (isAlreadyAssigned)
      throw new ValidationException("User already has the role");

    // create the mapping
    await UserRole.create(mappingData);
  }

  async removeUserFromRole(mappingData) {
    const role = await this.getOneRole(mappingData.role_id);
    const user = await userRepository.getOneUser(mappingData.user_id);

    // check if the mapping exists
    const isAssigned = await UserRole.findOne({
      where: mappingData,
    });
    if (isAssigned === null || isAssigned === undefined) {
      throw new ValidationException("User and Role mapping does not exist");
    }

    // delete the mapping
    await UserRole.destroy({
      where: mappingData,
    });
  }

  async addAccessToRole(mappingData) {
    const role = await this.getOneRole(mappingData.role_id);
    const module = await moduleService.getOneModule(mappingData.module_id);
    const privilege = await privilegeService.getOnePrivilege(
      mappingData.privilege_id
    );

    // check if the mapping between the `module` and `privilege` exists
    const modulePrivilege = await ModulePrivilege.findOne({
      where: {
        module_id: module.module_id,
        privilege_id: privilege.privilege_id,
      },
    });
    if (modulePrivilege === null || modulePrivilege === undefined) {
      throw new ValidationException("module privilege mapping does not exist");
    }

    // check if the `role` already has the access
    const access = await Access.findOne({
      where: {
        role_id: role.role_id,
        module_privilege_id: modulePrivilege.module_privilege_id,
      },
    });
    if (access) {
      throw new ValidationException("Role already has the access");
    }

    // create the mapping
    await Access.create({
      role_id: role.role_id,
      module_privilege_id: modulePrivilege.module_privilege_id,
    });
  }

  async removeAccessFromRole(mappingData) {
    const role = await this.getOneRole(mappingData.role_id);
    const module = await moduleService.getOneModule(mappingData.module_id);
    const privilege = await privilegeService.getOnePrivilege(
      mappingData.privilege_id
    );

    // check if the mapping between the `module` and `privilege` exists
    const modulePrivilege = await ModulePrivilege.findOne({
      where: {
        module_id: module.module_id,
        privilege_id: privilege.privilege_id,
      },
    });
    if (modulePrivilege === null || modulePrivilege === undefined) {
      throw new ValidationException("module privilege mapping does not exist");
    }

    // check if the `role` has the access
    const access = await Access.findOne({
      where: {
        role_id: role.role_id,
        module_privilege_id: modulePrivilege.module_privilege_id,
      },
    });
    if (access === null || access === undefined) {
      throw new ValidationException("Role does not have the access");
    }

    // delete the mapping
    await Access.destroy({
      where: {
        role_id: role.role_id,
        module_privilege_id: modulePrivilege.module_privilege_id,
      },
    });
  }

  async manageUserRoles(body) {
    const transaction = await sequelize.transaction();
    try {
      // delete all the user roles
      const { user_id } = body;
      await roleRepository.deleteUserRoles(transaction, user_id);

      // assign the user with roles
      const mappingData = body.roles.map((currValue) => {
        return {
          user_id: user_id,
          role_id: currValue.role_id,
        };
      });
      await roleRepository.assignUserRoles(transaction, mappingData);

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      if (err instanceof ForeignKeyConstraintError) {
        throw new ValidationException("Role doesnot exists");
      }
      throw err;
    }
  }

  async manageRoleAccess(body) {
    const transaction = await sequelize.transaction();
    try {
      // remove all the access from the role
      const { role_id } = body;
      await roleRepository.removeAccessFromRole(transaction, role_id);

      // assign access to the role
      const mappingData = body.module_privilege.map((currValue) => {
        return {
          role_id: role_id,
          module_privilege_id: currValue.module_privilege_id,
        };
      });
      await roleRepository.addAccessToRole(transaction, mappingData);

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      if (err instanceof ForeignKeyConstraintError) {
        throw new ValidationException(
          "Module Privilege mapping doesnot exists"
        );
      }
      throw err;
    }
  }
}

module.exports = new RoleService();
