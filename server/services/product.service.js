const { sequelize } = require("../configs/database.config");
const categoryService = require("./category.service");
const productRepository = require("../repositories/product.repository");
const { ValidationException } = require("../exceptions/validationException");

class ProductService {
  async createProduct(productData, files) {
    const transaction = await sequelize.transaction();
    try {
      // check if the `category` choosed for the `product` is not a parent-category
      // to enforce NoProductOnParentCategory
      const category = categoryService.getOneCategory(productData.category_id);
      if (category.parent_id === null) {
        throw new ValidationException(
          "Cannot create product on parent category"
        );
      }

      // create the product
      const product = await productRepository.createProduct(
        transaction,
        productData
      );

      // upload product-images to cloud
      const uploadResult = await productRepository.uploadToCloudinary(files);

      // bind images to the created product
      await productRepository.storeProductImagesMetadata(
        transaction,
        product.product_id,
        uploadResult
      );

      // add product to the meilisearch database
      await productRepository.addProductToSearchEngine(
        product.get({ plain: true })
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  }

  async getAllProducts(query) {
    try {
      const { categoryId } = query;

      // get products of category whose id is categoryId
      if (categoryId) {
        const category = await categoryService.getOneCategory(categoryId);
        const products = await productRepository.getAllProductsOfCategory(
          category
        );
        return products;
      } else {
        // get all products
        const products = await productRepository.getAllProducts();
        return products;
      }
    } catch (err) {
      throw err;
    }
  }

  async getOneProduct(productId) {
    try {
      const product = await productRepository.getOneProduct(productId);
      return product;
    } catch (err) {
      throw err;
    }
  }

  async updateProduct(params, productData) {
    try {
      const { productId } = params;

      const product = await productRepository.updateProduct(
        productId,
        productData
      );
      await productRepository.updateProductFromSearchEngine(
        product.get({ plain: true })
      );
    } catch (err) {
      throw err;
    }
  }

  async deleteProduct(params) {
    const transaction = await sequelize.transaction();
    try {
      const { productId } = params;

      // delete just the product from db
      await productRepository.deleteProduct(transaction, productId);

      // delete images of the product from cloud
      await productRepository.deleteImagesFromCloudinary(productId);

      // delete ProductImage mappings where product_id == null
      await productRepository.deleteImagesFromDB(transaction, productId);

      // delete product from the search engine
      await productRepository.deleteProductFromSearchEngine(productId);

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  }

  async addImagesToProduct(body, files) {
    const transaction = await sequelize.transaction();
    try {
      const { product_id } = body;

      const product = await productRepository.getOneProduct(product_id);

      const uploadResult = await productRepository.uploadToCloudinary(files);
      await productRepository.storeProductImagesMetadata(
        transaction,
        product.product_id,
        uploadResult
      );
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  }

  async deleteImageFromProduct(query) {
    try {
      const { productId, publicId } = query;
      await productRepository.deleteSingleImageOfProduct(productId, publicId);
    } catch (err) {
      throw err;
    }
  }
}

module.exports = new ProductService();
