const argon2 = require("argon2");
const jsonwebtoken = require("jsonwebtoken");
const AuthNException = require("../exceptions/authNException");
const { User, Role } = require("../models");

class AuthNService {
  async verifyLoginCredentials(email, password) {
    const fetchedUser = await this.fetchUserByEmail(email);
    if (fetchedUser == null) throw new AuthNException();

    const result = await this.verifyUserPassword(fetchedUser, password);
    if (!result) {
      throw new AuthNException();
    }

    const user = {
      id: fetchedUser.id,
      email: fetchedUser.email,
      username: fetchedUser.username,
      roles: [],
    };
    for (let i = 0; i < fetchedUser.roles.length; i++) {
      user.roles.push(fetchedUser.roles[i].role_name);
    }

    return user;
  }

  async issueJwtToken(user) {
    const payload = {
      id: user.id,
      email: user.email,
      username: user.username,
      roles: user.roles,
      iat: Date.now(),
    };

    const signedToken = jsonwebtoken.sign(payload, process.env.JWT_SECRET_KEY, {
      expiresIn: "1d",
    });

    return signedToken;
  }

  async verifyUserPassword(user, password) {
    return await argon2.verify(user.password, password);
  }

  async fetchUserByEmail(email) {
    const user = await User.scope(null).findOne({
      where: {
        email: email,
      },
      include: [
        {
          model: Role,
          as: "roles",
          attributes: ["role_name"],
          through: { attributes: [] },
        },
      ],
    });

    return user;
  }
}

module.exports = new AuthNService();
