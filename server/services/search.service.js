const searchRepository = require("../repositories/search.repository");

class SearchService {
  async search(query) {
    try {
      const { q } = query;
      const data = await searchRepository.search(q);
      return data;
    } catch (err) {
      throw err;
    }
  }
}

module.exports = new SearchService();
