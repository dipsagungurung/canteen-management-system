const express = require("express");
const cors = require("cors");
const morgan = require("morgan");

// Loading the .env file
require("dotenv").config();

const { sequelize } = require("./configs/database.config");
const { router } = require("./routes");
const { passport } = require("./configs/passport.config");
const { HttpException } = require("./exceptions/httpException");
const { printAllRegisteredRoutes } = require("./utils/helper.util");

/**
 *
 * ------------- GENERAL SETUP -------------
 *
 */
// Creating the express application
const app = express();

PORT = process.env.APP_PORT || 8080;

app.use(cors()); // for cross origin resource sharing
app.use(morgan("dev")); // for logging
app.use(express.json()); // for parsing json body
app.use(express.urlencoded({ extended: true })); // for parsing urlencoded payloads

/**
 *
 * ------------- DATABASE CONNECTION -------------
 *
 */
(async function () {
  try {
    await sequelize.authenticate();
    console.log("Database connected");
    // await sequelize.sync({ force: true });
  } catch (error) {
    console.error("Unable to connect to the database:", error);
  }
})();

/**
 *
 * ------------- PASSPORT SETUP -------------
 *
 */
app.use(passport.initialize());

/**
 *
 * ------------- APP ROUTES -------------
 *
 */
app.use(router);

/**
 *
 * ------------- ERROR HANDLERS -------------
 *
 */
// 404 error handler
app.use((req, res, next) => {
  const err = new HttpException(404, "Route doesnot exist");

  next(err);
});

// Global error handler
app.use((err, req, res, next) => {
  err.success = false;
  err.status = err.status || 500;
  err.message = err.message || "Something went wrong";
  err.data = err.data || err.stack || null;

  res.status(err.status).json({
    success: err.success,
    status: err.status,
    message: err.message,
    data: err.data,
  });
});

/**
 *
 * -------- LOG ALL THE ROUTES --------
 *
 */
printAllRegisteredRoutes(app);

/**
 *
 * ------------- APP PORT -------------
 *
 */
app.listen(PORT, () => {
  console.log(`
Server Listening:
-----------------

    *************************
    http://localhost:${PORT}/
    *************************
    `);
});
